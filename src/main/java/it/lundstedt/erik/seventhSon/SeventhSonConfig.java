package it.lundstedt.erik.seventhSon;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.cloth.clothconfig.shadowed.blue.endless.jankson.Comment;

import java.util.ArrayList;
import java.util.List;

@Config(name = SeventhSonMain.MODID)
public class SeventhSonConfig implements ConfigData {

	@Comment("If true fireballs always strike lightning when they hit something.")
	public boolean fireballsAlwaysLightning = false;


	@Comment("The magical words that turns text into a spell any of them are at the very beginning of it.")
	public List<String> lispHeaders = new ArrayList<>();

	@Comment("Client side only settings, have no effect on servers.")
	@ConfigEntry.Gui.CollapsibleObject
	public Client client = new Client();


	public static class Client {

		@Comment("If true, the command-line command bellow will be used instead of the operating-system default when opening text files in external software externally from this mod.")
		public boolean useCustomOpenTextFileCommand = false;

		@Comment("Command line command used when useCustomOpenTextFileCommand is true.")
		public String customOpenTextFileCommand = "x-terminal-emulator /etc/alternatives/editor";

		@Comment("If true, using escape in the lisp machine will save the changes to the block. If false, it will erase all progress unless you press the save button.")
		public boolean saveLispMachineOnGUIExit = true;


		@Comment("Mana display options.")
		@ConfigEntry.Gui.CollapsibleObject
		public ManaDisplaySettings manaDisplaySettings = new ManaDisplaySettings();

		public static class ManaDisplaySettings {
			@Comment("If true, mana will be displayed on a progress bar in-game. If false, you can't see your mana.")
			public boolean enableBar = true;

			@Comment("")
			public int xOffset = -20;

			@Comment("")
			public int yOffset = 10;

			public enum XAlignment {
				LEFT,
				MIDDLE,
				RIGHT
			}

			public enum YAlignment {
				TOP,
				MIDDLE,
				BOTTOM
			}

			@Comment("")
			public XAlignment xAlignment = XAlignment.RIGHT;

			@Comment("")
			public YAlignment yAlignment = YAlignment.BOTTOM;
		}
	}

}
