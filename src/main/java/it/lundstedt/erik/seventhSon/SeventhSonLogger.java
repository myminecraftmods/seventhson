package it.lundstedt.erik.seventhSon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SeventhSonLogger {

	public static final Logger logger = LogManager.getLogger(SeventhSonMain.MODID);

	public static void info(Object info) {
		logger.info(info);
	}

	public static void error(Object error) {
		logger.error(error);
	}

	public static void fatal(Object fatal) {
		logger.fatal(fatal);
	}

}
