package it.lundstedt.erik.seventhSon;

import it.lundstedt.erik.seventhSon.common.Events;
import it.lundstedt.erik.seventhSon.common.block_entities.SeventhSonBlockEntities;
import it.lundstedt.erik.seventhSon.common.blocks.SeventhSonBlocks;
import it.lundstedt.erik.seventhSon.common.items.SeventhSonItems;
import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.registries.SpellFunctionRegistry;
import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.common.screen_handler.SeventhSonScreenHandlers;
import it.lundstedt.erik.seventhSon.network.ServerNetworkHandler;
import it.lundstedt.erik.seventhSon.common.registries.TextRegistryInternal;
import it.lundstedt.erik.seventhSon.util.ConfigCommons;
import it.lundstedt.erik.seventhSon.util.helper.CompatHelper;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.JanksonConfigSerializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import org.armedbear.lisp.Lisp;


public class SeventhSonMain implements ModInitializer {

	public static final String MODID = "seventh_son";

	public static SeventhSonConfig config;

	@Override
	public void onInitialize() {
		AutoConfig.register(SeventhSonConfig.class, JanksonConfigSerializer::new);
		AutoConfig.getConfigHolder(SeventhSonConfig.class).registerLoadListener((holder, config) -> {
			ConfigCommons.sync(config);
			ConfigCommons.shouldSync = true;
			return ActionResult.PASS;
		});
		AutoConfig.getConfigHolder(SeventhSonConfig.class).registerSaveListener((holder, config) -> {
			ConfigCommons.sync(config);
			ConfigCommons.shouldSync = true;
			return ActionResult.PASS;
		});
		config = AutoConfig.getConfigHolder(SeventhSonConfig.class).getConfig();

		if (!FabricLoader.getInstance().getConfigDir().resolve(MODID + ".json5").toFile().exists())  {
			//TODO Make this reset properly.
			config.lispHeaders.add("mode: lisp");
			config.lispHeaders.add("mode:lisp");
			AutoConfig.getConfigHolder(SeventhSonConfig.class).save();
		}

		SeventhSonScreenHandlers.init();
		SeventhSonBlocks.init();
		SeventhSonBlockEntities.init();
		SeventhSonItems.init();

		TypeRegistry.init();
		ActionRegistry.init();
		SpellFunctionRegistry.init();
		TextRegistryInternal.init();
		
		Events.init();
		ServerNetworkHandler.init();

		CompatHelper.init();
	}

	public static Identifier getId(String input) {
		return new Identifier(MODID, input);
	}

	public static String getTranslationString(String type, String object, String text) {
		String dot = ".";
		return type + dot + MODID + dot + object + dot + text;
	}

	public static TranslatableText getTranslatableText(String type, String object, String text, Object... args) {
		return new TranslatableText(getTranslationString(type, object, text), args);
	}

	public static String getTranslationString(String type, String text) {
		String dot = ".";
		return type + dot + MODID + dot + text;
	}

	public static TranslatableText getTranslatableText(String type, String text, Object... args) {
		return new TranslatableText(getTranslationString(type, text), args);
	}

}
