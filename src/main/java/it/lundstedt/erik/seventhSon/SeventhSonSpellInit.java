package it.lundstedt.erik.seventhSon;

import it.lundstedt.erik.seventhSon.common.spell_interfaces.Vectorizable;
import it.lundstedt.erik.seventhSon.common.spell_objects.LispHitResult;
import it.lundstedt.erik.seventhSon.common.spell_objects.LispVec3d;
import it.lundstedt.erik.seventhSon.userFriendly.Functions;
import it.lundstedt.erik.seventhSon.userFriendly.LispPackages;
import it.lundstedt.erik.seventhSon.util.InitSpells;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;
import org.armedbear.lisp.*;

import java.util.function.UnaryOperator;

import static org.armedbear.lisp.Lisp.error;

public class SeventhSonSpellInit implements InitSpells {




	@Override
	public void initSpells(RegistrationFunction registry) {
		registry.register("raycast", context -> new Function("raycast","") {

			@Override
			public LispObject execute() {
				return execute(new LispObject[]{});
			}

			@Override
			public LispObject execute(LispObject arg) {
				return execute(new LispObject[]{arg});
			}

			@Override
			public LispObject execute(LispObject arg1, LispObject arg2) {
				return execute(new LispObject[]{arg1, arg2});
			}

			@Override
			public LispObject execute(LispObject arg1, LispObject arg2, LispObject arg3) {
				return execute(new LispObject[]{arg1, arg2, arg3});
			}

			@Override
			public LispObject execute(LispObject[] params) {
				/**
				 * arguments:
				 * double maxDistance; default=6
				 * shapeType; must be one of
				 *                  COLLIDER
				 *                  OUTLINE
				 *                  VISUAL
				 * fluidHandling; must be one of:
				 *                  NONE
				 *                  SOURCE_ONLY
				 *                  ANY
				 */

				Entity entity = context.caster.getEntity().orElse(null);
				if (entity != null) {
					double maxDistance = 6;
					if (params.length >= 1) {
						maxDistance = params[0].doubleValue();
					}

					RaycastContext.ShapeType shapeType = RaycastContext.ShapeType.COLLIDER;
					if (params.length >= 2) {
						shapeType = RaycastContext.ShapeType.valueOf(params[1].toString());
					}

					RaycastContext.FluidHandling fluidHandling = RaycastContext.FluidHandling.NONE;

					if (params.length >= 3) {
						fluidHandling = RaycastContext.FluidHandling.valueOf(params[2].toString());
					}

					UnaryOperator<Double> drainFunction = dist -> dist * 0.25;

					maxDistance = context.modifyBasedOnMana(maxDistance, drainFunction.apply(maxDistance), true);

					Vec3d start = entity.getPos().add(0, entity.getEyeHeight(entity.getPose()), 0);
					Vec3d facing = entity.getRotationVector();
					Vec3d end = start.add(facing.multiply(maxDistance));

					HitResult result = entity.world.raycast(new RaycastContext(start, end, shapeType, fluidHandling, entity));
					if (result.getType().equals(HitResult.Type.MISS)) {
						RaycastContext.ShapeType finalShape = shapeType;
						EntityHitResult entityHitResult = ProjectileUtil.getEntityCollision(entity.world, entity, start, end, entity.getBoundingBox().stretch(facing).expand(1.0D), hitEntity -> {
							switch (finalShape) {
								case COLLIDER:
									return hitEntity.collides();
								case VISUAL:
									return !hitEntity.isInvisible();
								case OUTLINE:
									return true;
								default:
									return false;
							}
						});
						if (entityHitResult != null && entityHitResult.getType().equals(HitResult.Type.ENTITY)) {
							result = entityHitResult;
						}
					}
					context.drainMana(drainFunction.apply(result.getPos().distanceTo(entity.getPos())), false);
					return new LispHitResult(result);
				} else {
					return LispHelper.errorNotEntity();
				}
			}
		});
		registry.register("smite", context -> new Function("smite", "") {
			@Override
			public LispObject execute() {
				return execute(new LispObject[]{});
			}

			@Override
			public LispObject execute(LispObject arg) {
				return execute(new LispObject[]{arg});
			}

			@Override
			public LispObject execute(LispObject[] params) {
				Vec3d pos = context.caster.getPos();
				if (params.length >= 1 && params[0] instanceof LispVec3d) {
					pos = ((LispVec3d)params[0]).javaInstance();
				}
				if (!context.drainManaIfPossible(pos.distanceTo(context.caster.getPos()) + 75, false)) return Lisp.NIL;
				LightningEntity entity = EntityType.LIGHTNING_BOLT.create(context.caster.getWorld());
				context.caster.getPlayer().ifPresent(entity::setChanneler);
				entity.setCosmetic(false);
				entity.refreshPositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
				context.caster.getWorld().spawnEntity(entity);
				return Lisp.NIL;
			}
		});

		registry.register("heal", context -> new Function("heal") {
			@Override
			public LispObject execute() {
				return execute(new LispObject[]{});
			}

			@Override
			public LispObject execute(LispObject param) {
				return execute(new LispObject[]{param});
			}

			@Override
			public LispObject execute(LispObject[] params) {
				LivingEntity entity = context.caster.getLivingEntity().orElse(null);
				if (entity != null) {
					float toHeal = 10;
					if (params.length >= 1) {
						toHeal = params[0].floatValue();
					}
					float actuallyHealed = Math.min(toHeal, entity.getMaxHealth()-entity.getHealth());
					actuallyHealed = (float) context.modifyBasedOnMana(actuallyHealed, actuallyHealed * 10, false);
					entity.heal(actuallyHealed);
					return SingleFloat.getInstance(actuallyHealed);
				} else {
					return LispHelper.errorNotLivingEntity();
				}
			}
		});

		registry.register("Vec3d/get", context -> new Function("Vec3d/get", "") {

			@Override
			public LispObject execute(LispObject arg) {
				if (arg instanceof Vectorizable) {
					return ((Vectorizable)arg).getVec3d();
				}
				return Lisp.NIL;
			}


		});

		registry.register("HitResult/getType", context -> new Function("HitResult/getType", "") {

			@Override
			public LispObject execute(LispObject arg) {
				if (arg instanceof LispHitResult) {
					return ((LispHitResult)arg).getType();
				}
				return Lisp.NIL;
			}

		});

		registry.register("HitResult/Type/BLOCK", context -> new SimpleString(HitResult.Type.BLOCK.name()));
		registry.register("HitResult/Type/ENTITY", context -> new SimpleString(HitResult.Type.ENTITY.name()));
		registry.register("HitResult/Type/MISS", context -> new SimpleString(HitResult.Type.MISS.name()));


		registry.register("user/getPos", context -> new Function("user/getPos") {

			@Override
			public LispObject execute() {
				return LispVec3d.fromVec3d(context.caster.getPos());
			}

		});

		registry.register("self/getPos", context -> new Function("self/getPos") {

			@Override
			public LispObject execute() {
				LispVec3d vec3d = context.getPosOfSelf().map(LispVec3d::fromVec3d).orElse(null);
				return vec3d != null ? vec3d : Lisp.NIL;
			}

		});



		LispPackages packageList=new LispPackages();
		Functions.initSpells(registry, packageList);




	}
}
