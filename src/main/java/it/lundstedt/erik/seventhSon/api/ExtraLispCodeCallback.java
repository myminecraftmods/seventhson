package it.lundstedt.erik.seventhSon.api;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.common.registries.TextRegistryInternal;
import it.lundstedt.erik.seventhSon.util.BlockReference;
import it.lundstedt.erik.seventhSon.util.EntityOrBlockEntity;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Unit;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface ExtraLispCodeCallback<T> {

	static <T> Event<ExtraLispCodeCallback<T>> getCallback(Class<T> object) {
		return (Event<ExtraLispCodeCallback<T>>) (Object) TextRegistryInternal.registry.get(object).callback;
	}
	
	static Event<ExtraLispCodeCallback<ItemStack>> getItemCallback() {
		return getCallback(ItemStack.class);
	}

	static Event<ExtraLispCodeCallback<BlockReference>> getBlockCallback() {
		return getCallback(BlockReference.class);
	}

	static Event<ExtraLispCodeCallback<Entity>> getEntityCallback() {
		return getCallback(Entity.class);
	}

	Event<ExtraLispCodeCallback<Unit>> DEFAULT_CALLBACK = new Event<ExtraLispCodeCallback<Unit>>() {
		@Override
		public void register(ExtraLispCodeCallback<Unit> listener) {
			TextRegistryInternal.registry.forEach((objectClass, entry) -> {
				entry.callback.register((caster, object) -> listener.getAdditionalCode(caster, Unit.INSTANCE));
			});
		}
	};

	String[] getAdditionalCode(EntityOrBlockEntity caster, T object);

}
