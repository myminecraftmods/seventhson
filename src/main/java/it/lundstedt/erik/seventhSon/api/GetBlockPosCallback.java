package it.lundstedt.erik.seventhSon.api;

import com.mojang.datafixers.util.Either;
import it.lundstedt.erik.seventhSon.util.EntityOrBlockEntity;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public interface GetBlockPosCallback {

	Event<GetBlockPosCallback> EVENT = EventFactory.createArrayBacked(GetBlockPosCallback.class, listeners -> (object, caster, pos) -> {
		for (GetBlockPosCallback callback : listeners) {
			pos = callback.getPos(object, caster, pos);
		}
		return pos;
	});

	BlockPos getPos(Object object, EntityOrBlockEntity caster, BlockPos lastPos);

}
