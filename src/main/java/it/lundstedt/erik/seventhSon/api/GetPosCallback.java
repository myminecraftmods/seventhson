package it.lundstedt.erik.seventhSon.api;

import com.mojang.datafixers.util.Either;
import it.lundstedt.erik.seventhSon.common.registries.TextRegistryInternal;
import it.lundstedt.erik.seventhSon.util.EntityOrBlockEntity;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.Unit;
import net.minecraft.util.math.Vec3d;

public interface GetPosCallback {

	Event<GetPosCallback> EVENT = EventFactory.createArrayBacked(GetPosCallback.class, listeners -> (object, caster, pos) -> {
		for (GetPosCallback callback : listeners) {
			pos = callback.getPos(object, caster, pos);
		}
		return pos;
	});

	Vec3d getPos(Object object, EntityOrBlockEntity caster, Vec3d lastPos);

}
