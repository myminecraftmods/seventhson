package it.lundstedt.erik.seventhSon.api;

import net.minecraft.entity.Entity;

@FunctionalInterface
public interface TextGetter<T> {

	String[] getText(T object);

	default double getManaFactor(T object) {
		return 1;
	}

}
