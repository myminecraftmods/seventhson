package it.lundstedt.erik.seventhSon.api;

import it.lundstedt.erik.seventhSon.common.registries.TextRegistryInternal;
import it.lundstedt.erik.seventhSon.util.BlockReference;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public interface TextRegistry {

	TextRegistry INSTANCE = new TextRegistryInternal();

	static TextRegistry getInstance() {
		return INSTANCE;
	}

	default void registerItem(Item validItem, TextGetter<ItemStack> itemText) {
		registerItem(item -> validItem.equals(item.getItem()), itemText);
	}

	default void registerItem(Predicate<ItemStack> validItem, TextGetter<ItemStack> itemText) {
		register(ItemStack.class, validItem, itemText);
	}

	default void registerBlock(Predicate<BlockReference> validBlock, TextGetter<BlockReference> blockText) {
		register(BlockReference.class, validBlock, blockText);
	}

	default void registerEntity(Predicate<Entity> validEntity, TextGetter<Entity> entityText) {
		register(Entity.class, validEntity, entityText);
	}

	<T> void register(Class<T> type, Predicate<T> valid, TextGetter<T> getter);

	<T> void registerType(Class<T> type, TextRegistryInternal.Entry<T> entry);

	default <T> void registerType(Class<T> type) {
		registerType(type, new TextRegistryInternal.Entry<>());
	}

	<T> Optional<TextGetter<T>> get(Class<T> objectClass, T object);

	default Optional<TextGetter<ItemStack>> get(ItemStack item) {
		return get(ItemStack.class, item);
	}

	default Optional<TextGetter<BlockReference>> get(World world, BlockPos pos) {
		return get(new BlockReference(world, pos));
	}

	default Optional<TextGetter<BlockReference>> get(BlockReference blockReference) {
		return get(BlockReference.class, blockReference);
	}

	default Optional<TextGetter<Entity>> get(Entity entity) {
		return get(Entity.class, entity);
	}

}
