package it.lundstedt.erik.seventhSon.client;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.util.helper.FileHelper;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.world.CreateWorldScreen;
import net.minecraft.util.Identifier;
import net.minecraft.util.WorldSavePath;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.gen.GeneratorOptions;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class BlockEntityFileRegistry {

	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().serializeNulls().disableHtmlEscaping().create();

	private static final BlockEntityFileRegistry INSTANCE = new BlockEntityFileRegistry();

	private BlockEntityFileRegistry() {

	}

	protected Map<EnvType, Map<String, Map<RegistryKey<World>, Map<BlockPos, Path>>>> registry = new HashMap<>();

	public void addBlock(BlockEntityContext context, Path path) {
		if (path == null || path.getParent() == null) return;
		if (!registry.containsKey(context.envType)) {
			registry.put(context.envType, new HashMap<>());
		}
		Map<String, Map<RegistryKey<World>, Map<BlockPos, Path>>> sidedRegistry = registry.get(context.envType);
		if (!sidedRegistry.containsKey(context.name)) {
			sidedRegistry.put(context.name, new HashMap<>());
		}
		Map<RegistryKey<World>, Map<BlockPos, Path>> worldSpecificRegistry = sidedRegistry.get(context.name);
		if (!worldSpecificRegistry.containsKey(context.dimension)) {
			worldSpecificRegistry.put(context.dimension, new HashMap<>());
		}
		Map<BlockPos, Path> dimensionSpecificRegistry = worldSpecificRegistry.get(context.dimension);
		dimensionSpecificRegistry.put(context.position, path);
		path.getParent().toFile().mkdirs();
	}

	public void removeBlock(BlockEntityContext context) {
		if (registry.containsKey(context.envType)) {
			Map<String, Map<RegistryKey<World>, Map<BlockPos, Path>>> sidedRegistry = registry.get(context.envType);
			if (sidedRegistry.containsKey(context.name)) {
				Map<RegistryKey<World>, Map<BlockPos, Path>> worldSpecificRegistry = sidedRegistry.get(context.name);
				if (worldSpecificRegistry.containsKey(context.dimension)) {
					Map<BlockPos, Path> dimensionSpecificRegistry = worldSpecificRegistry.get(context.dimension);
					if (dimensionSpecificRegistry.remove(context.position) != null) {
						if (dimensionSpecificRegistry.isEmpty()) {
							worldSpecificRegistry.remove(context.dimension);
							if (worldSpecificRegistry.isEmpty()) {
								sidedRegistry.remove(context.name);
								if (sidedRegistry.isEmpty()) {
									registry.remove(context.envType);
								}
							}
						}
					}
				}
			}
		}
	}

	public Optional<Path> getPathFromBlock(BlockEntityContext context) {
		return Optional.ofNullable(registry.getOrDefault(context.envType, new HashMap<>()).getOrDefault(context.name, new HashMap<>()).getOrDefault(context.dimension, new HashMap<>()).get(context.position));
	}

	public BlockEntityContext getContext(MinecraftClient client, BlockPos pos) {
		EnvType type = client.isIntegratedServerRunning() ? EnvType.CLIENT : EnvType.SERVER;
		String name = "ERROR";
		switch (type) {
			case CLIENT:
				Path path = client.getServer().getSavePath(WorldSavePath.ROOT);
				name = path.toFile().getName();
				if (name.equals(".")) {
					name = path.getParent().toFile().getName();
				}
				break;
			case SERVER:
				name = client.getCurrentServerEntry().name;
				break;
		}
		RegistryKey<World> dimension = client.world.getRegistryKey();
		return new BlockEntityContext(type, name, dimension, pos);
	}

	public Optional<Path> getPathFromBlock(MinecraftClient client, BlockPos position) {
		return getPathFromBlock(getContext(client, position));
	}

	public void addBlock(MinecraftClient client, BlockPos position, Path path) {
		addBlock(getContext(client, position), path);
	}

	public void removeBlock(MinecraftClient client, BlockPos position) {
		removeBlock(getContext(client, position));
	}

	public void writeToFile(MinecraftClient client, BlockPos position) {
		getPathFromBlock(client, position).ifPresent(path -> {
			BlockEntity block = client.world.getBlockEntity(position);
			if (block instanceof PathAble) {
				PathAble pathAble = ((PathAble) block);
				if (!path.getParent().toFile().exists()) {
					removeBlock(client, position);
					return;
				}
				try {
					File file = path.toFile();
					if (!file.exists()) {
						file.createNewFile();
					}
					FileWriter writer = new FileWriter(file);
					try {
						for (String line : pathAble.getContents()) {
							writer.write(line);
							writer.write(System.lineSeparator());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void readFromFile(MinecraftClient client, BlockPos position) {
		readFromFile(client, position, lines -> {});
	}

	public void readFromFile(MinecraftClient client, BlockPos position, Consumer<String[]> furtherTextActions) {
		getPathFromBlock(client, position).ifPresent(path -> {
			BlockEntity block = client.world.getBlockEntity(position);
			if (block instanceof PathAble) {
				PathAble pathAble = ((PathAble) block);
				if (!path.toFile().exists()) {
					removeBlock(client, position);
					return;
				}
				try {
					BufferedReader reader = new BufferedReader(new FileReader(path.toFile()));
					try {
						List<String> list = reader.lines().collect(Collectors.toList());
						for (int i = list.size()-1; i >= 0; i--) {
							if (list.get(i).isEmpty()) {
								list.remove(i);
							} else {
								break;
							}
						}
						String[] lines = list.toArray(new String[]{});
						furtherTextActions.accept(lines);
						pathAble.putContents(lines);
					} catch (Exception e) {
						e.printStackTrace();
					}
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void saveRegistry() {
		Path startingPath = FabricLoader.getInstance().getConfigDir().resolve(SeventhSonMain.MODID);
		for (Map.Entry<EnvType, Map<String, Map<RegistryKey<World>, Map<BlockPos, Path>>>> sidedRegistry : registry.entrySet()) {
			Path sidePath = startingPath.resolve(sidedRegistry.getKey().name().toLowerCase(Locale.ROOT));
			for (Map.Entry<String, Map<RegistryKey<World>, Map<BlockPos, Path>>> worldSpecificRegistry : sidedRegistry.getValue().entrySet()) {
				Path worldPath = sidePath.resolve(worldSpecificRegistry.getKey());
				for (Map.Entry<RegistryKey<World>, Map<BlockPos, Path>> dimensionSpecificRegistry : worldSpecificRegistry.getValue().entrySet()) {
					Identifier id = dimensionSpecificRegistry.getKey().getValue();
					Path dimensonPath = worldPath.resolve(id.getNamespace()).resolve(id.getPath() + ".json");
					File folder = dimensonPath.getParent().toFile();
					if (!folder.exists()) {
						folder.mkdirs();
					}
					positionMapCodec.encodeStart(JsonOps.INSTANCE, dimensionSpecificRegistry.getValue()).resultOrPartial(SeventhSonLogger::error).ifPresent(json -> {
						try {
							JsonWriter jsonWriter = GSON.newJsonWriter(Files.newBufferedWriter(dimensonPath, StandardCharsets.UTF_8));
							Throwable var4 = null;

							try {
								GSON.toJson(json, jsonWriter);
							} catch (Throwable var14) {
								var4 = var14;
								throw var14;
							} finally {
								if (jsonWriter != null) {
									if (var4 != null) {
										try {
											jsonWriter.close();
										} catch (Throwable var13) {
											var4.addSuppressed(var13);
										}
									} else {
										jsonWriter.close();
									}
								}

							}
						} catch (JsonIOException | IOException e) {
							e.printStackTrace();
						}
					});
				}
			}
		}
	}

	public void loadRegistry() {
		registry.clear();
		Path startingPath = FabricLoader.getInstance().getConfigDir().resolve(SeventhSonMain.MODID);
		File startingFile = startingPath.toFile();
		JsonParser jsonParser = new JsonParser();
		if (startingFile.exists()) {
			for (File sideFolder : Optional.ofNullable(startingFile.listFiles((dir, name) -> {
				try {
					EnvType.valueOf(name.toUpperCase(Locale.ROOT));
					return true;
				} catch (Exception e) {
					return false;
				}
			})).orElse(new File[]{})) {
				EnvType side = EnvType.valueOf(sideFolder.getName().toUpperCase(Locale.ROOT));
				for (File worldFolder : Optional.ofNullable(sideFolder.listFiles()).orElse(new File[0])) {
					String name = worldFolder.getName();
					int dimensionPathStart = worldFolder.toPath().getNameCount();
					for (File dimensionFile : FileHelper.listFilesRecursively(worldFolder)) {
						if (dimensionFile.isDirectory() || !dimensionFile.getName().endsWith(".json")) continue;
						Path dimensionPath = dimensionFile.toPath();
						String dimId = FileHelper.writePathToString(dimensionPath.subpath(dimensionPathStart, dimensionPath.getNameCount())).replaceFirst("/", ":");
						dimId = dimId.substring(0, dimId.length()-5);
						RegistryKey<World> dimension = RegistryKey.of(Registry.DIMENSION, new Identifier(dimId));
						try {
							BufferedReader bufferedReader = Files.newBufferedReader(dimensionPath);
							Throwable var13 = null;

							try {
								JsonElement jsonElement = jsonParser.parse(bufferedReader);
								positionMapCodec.parse(JsonOps.INSTANCE, jsonElement).resultOrPartial(SeventhSonLogger::error).ifPresent(positionRegistry -> {
									positionRegistry.forEach((pos, path) -> {
										addBlock(new BlockEntityContext(side, name, dimension, pos), path);
									});
								});
							} catch (Throwable var24) {
								var13 = var24;
								throw var24;
							} finally {
								if (bufferedReader != null) {
									if (var13 != null) {
										try {
											bufferedReader.close();
										} catch (Throwable var23) {
											var13.addSuppressed(var23);
										}
									} else {
										bufferedReader.close();
									}
								}

							}
						} catch (JsonIOException | JsonSyntaxException | IOException var27) {
							var27.printStackTrace();
						}
					}
				}
			}
		}
	}

	public static BlockEntityFileRegistry getInstance() {
		return INSTANCE;
	}

	/*public List<LispMachineEntry> registry2;

	public static class LispMachineEntry {
		public EnvType envType;
		public String name;
		public RegistryKey<World> dimension;
		public BlockPos position;
		public Path path;

		public LispMachineEntry(EnvType envType, String name, RegistryKey<World> dimension, BlockPos position, Path path) {
			this.envType = envType;
			this.name = name;
			this.dimension = dimension;
			this.position = position;
			this.path = path;
		}
	}*/

	public static class BlockEntityContext {
		public EnvType envType;
		public String name;
		public RegistryKey<World> dimension;
		public BlockPos position;

		public BlockEntityContext(EnvType envType, String name, RegistryKey<World> dimension, BlockPos position) {
			this.envType = envType;
			this.name = name;
			this.dimension = dimension;
			this.position = position;
		}
	}

	public static Codec<Pair<BlockPos, Path>> positionPairCodec = RecordCodecBuilder.create(instance -> {
		return instance.group(
				BlockPos.CODEC.fieldOf("position").forGetter(Pair::getFirst),
				Codec.STRING.xmap(Paths::get, FileHelper::writePathToString).fieldOf("path").forGetter(Pair::getSecond)
		).apply(instance, Pair::of);
	});

	public static Codec<Map<BlockPos, Path>> positionMapCodec = positionPairCodec.listOf().xmap(
			list -> list.stream().collect(Collectors.toMap(Pair::getFirst, Pair::getSecond)),
			map -> map.entrySet().stream().map(entry -> Pair.of(entry.getKey(), entry.getValue())).collect(Collectors.toList()));

	public interface PathAble {
		String[] getContents();
		void putContents(String[] newContents);
	}

}
