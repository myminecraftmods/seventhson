package it.lundstedt.erik.seventhSon.client;

import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import it.lundstedt.erik.seventhSon.SeventhSonConfig;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import me.shedaniel.autoconfig.AutoConfig;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class ConfigMenu implements ModMenuApi {

	@Override
	public ConfigScreenFactory<?> getModConfigScreenFactory() {
		return parent -> AutoConfig.getConfigScreen(SeventhSonConfig.class, parent).get();
	}
}
