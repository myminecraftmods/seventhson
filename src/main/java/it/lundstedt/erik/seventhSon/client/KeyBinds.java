package it.lundstedt.erik.seventhSon.client;

import it.lundstedt.erik.seventhSon.network.ClientNetworkHandler;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;

public class KeyBinds {


	public static KeyBinding triggerSpell = new KeyBinding("key.seventh-son.trigger-spell", 162, "key.categories.misc");

	public static void init() {
		KeyBindingHelper.registerKeyBinding(triggerSpell);
	}

	public static void tick() {
		if (triggerSpell.wasPressed()) {
			PlayerEntity player = MinecraftClient.getInstance().player;
			if (player != null) {
				Vec3d eyeHeight = player.getPos().add(0, player.getEyeHeight(player.getPose()), 0);
				Vec3d facing = player.getRotationVector().multiply(6);
				Vec3d target = eyeHeight.add(facing);
				HitResult result = ProjectileUtil.getEntityCollision(player.world, player, eyeHeight, target, player.getBoundingBox().stretch(facing).expand(1.0D), entity -> true);
				if (result != null && result.getType().equals(HitResult.Type.ENTITY)) {
					Entity entity = ((EntityHitResult)result).getEntity();
					if (LispHelper.parseLisp(player, entity)) {
						ClientNetworkHandler.sendPacket(ClientNetworkHandler.activateSpell(entity));
						return;
					}
				}
				result = player.world.raycast(new RaycastContext(eyeHeight, target, RaycastContext.ShapeType.OUTLINE, RaycastContext.FluidHandling.NONE, player));
				if (result.getType().equals(HitResult.Type.BLOCK)) {
					BlockPos pos = ((BlockHitResult)result).getBlockPos();
					if (LispHelper.parseLisp(player, player.world, pos)) {
						ClientNetworkHandler.sendPacket(ClientNetworkHandler.activateSpell(pos));
						return;
					}
				}
				if (LispHelper.parseLisp(player, player.getMainHandStack())) {
					ClientNetworkHandler.sendPacket(ClientNetworkHandler.activateSpell(player.inventory.selectedSlot));
					return;
				}
				if (LispHelper.parseLisp(player, player.getOffHandStack())) {
					ClientNetworkHandler.sendPacket(ClientNetworkHandler.activateSpell(player.inventory.main.size() + player.inventory.armor.size()));
					return;
				}
			}
		}
	}


}
