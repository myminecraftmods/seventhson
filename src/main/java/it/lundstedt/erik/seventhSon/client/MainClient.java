package it.lundstedt.erik.seventhSon.client;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.client.gui.SeventhSonGui;
import it.lundstedt.erik.seventhSon.component.PlayerManaComponent;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;

import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

public class MainClient implements ClientModInitializer {
	@Override
	public void onInitializeClient() {
		KeyBinds.init();
		SeventhSonGui.init();

		BlockEntityFileRegistry.getInstance().loadRegistry();

		ClientLifecycleEvents.CLIENT_STOPPING.register(client -> {
			BlockEntityFileRegistry.getInstance().saveRegistry();
		});
		ServerLifecycleEvents.SERVER_STOPPING.register(server -> {
			BlockEntityFileRegistry.getInstance().saveRegistry();
		});

		HudRenderCallback.EVENT.register((matrixStack, tickDelta) -> {
			MinecraftClient client = MinecraftClient.getInstance();

			PlayerManaComponent.getComponent(client.player).ifPresent(component -> {
				if (component.displayMana()) {
					if (SeventhSonMain.config.client.manaDisplaySettings.enableBar) {
						matrixStack.push();

						int xOffset = SeventhSonMain.config.client.manaDisplaySettings.xOffset;
						int yOffset = SeventhSonMain.config.client.manaDisplaySettings.yOffset;


						int width = client.getWindow().getScaledWidth();
						int height = client.getWindow().getScaledHeight();
						MutableText text = SeventhSonMain.getTranslatableText("hud", "mana").formatted(Formatting.RESET);
						float scale = 0.65f;
						float matrixScale = scale;
						matrixStack.scale(matrixScale, matrixScale, matrixScale);
						int barWidth = 182;

						/*double testValue = Math.pow(scale, 2);
						testValue = Math.sqrt(testValue*scale);

						float testScale = (float) testValue;*/

						int xStartPos = 0;
						switch (SeventhSonMain.config.client.manaDisplaySettings.xAlignment) {
							case MIDDLE:
								xStartPos = Math.round(width/2f);
								break;
							case RIGHT:
								xStartPos = Math.round(width);
								break;
						}
						int actualXStartPos = xStartPos;
						BiFunction<Integer, Integer, Integer> xGetter = (objectWidth, offset) -> Math.round((actualXStartPos - objectWidth / 2f)/scale) + offset;

						int yStartPos = 0;
						switch (SeventhSonMain.config.client.manaDisplaySettings.yAlignment) {
							case MIDDLE:
								yStartPos = Math.round((height/2f)/scale);
								break;
							case BOTTOM:
								yStartPos = Math.round(height/scale);
								break;
						}
						int y = yStartPos - yOffset;

						client.textRenderer.drawWithShadow(matrixStack, text, xGetter.apply(Math.round(client.textRenderer.getWidth(text)/scale), Math.round(xOffset - (barWidth/2f)*scale)), y - 10, 0xffffff);

						client.getTextureManager().bindTexture(new Identifier("textures/gui/bars.png"));

						int x = xGetter.apply(barWidth, xOffset);
						DrawableHelper.drawTexture(matrixStack, x, y, 0, 10, barWidth, 5, 256, 256);
						DrawableHelper.drawTexture(matrixStack, x, y, 0, 15, (int)Math.round(MathHelper.lerp(tickDelta, (component.getPrevMana()/component.maxMana) * barWidth, (component.getMana()/component.maxMana) * barWidth)), 5, 256, 256);
						matrixStack.pop();
					}
				}
			});
		});
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			if (client.player != null && client.world != null) {
				KeyBinds.tick();
			}
		});
	}
}
