package it.lundstedt.erik.seventhSon.client.gui;

import com.mojang.datafixers.util.Pair;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.block_entities.LispMachineBlockEntity;
import it.lundstedt.erik.seventhSon.network.ClientNetworkHandler;
import me.lambdaurora.spruceui.screen.SpruceScreen;
import me.lambdaurora.spruceui.Position;
import me.lambdaurora.spruceui.widget.SpruceButtonWidget;
import me.lambdaurora.spruceui.widget.SpruceLabelWidget;
import me.lambdaurora.spruceui.widget.container.SpruceContainerWidget;
import me.lambdaurora.spruceui.widget.text.SpruceTextAreaWidget;
import me.lambdaurora.spruceui.widget.text.SpruceTextFieldWidget;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;

import java.util.Arrays;


public class LispMachineScreen extends SpruceScreen implements PathSettingsScreen.Retextable {
	private final Screen parent;
    private SpruceTextAreaWidget textArea;

	private SpruceLabelWidget parensCount;

	public LispMachineBlockEntity blockEntity;

	public LispMachineScreen(LispMachineBlockEntity blockEntity) {
		this(blockEntity, null);
	}

	public LispMachineScreen(LispMachineBlockEntity blockEntity, @Nullable Screen parent) {
		super(SeventhSonMain.getTranslatableText("screen", "lisp_machine", "title"));
		this.parent = parent;
		this.blockEntity = blockEntity;
	}


	public static Pair<Integer,Integer> countParens(SpruceTextAreaWidget text) {
		int startParensCount = text.getText().split("[(]").length-1;
		int endParensCount = text.getText().split("[)]").length-1;
		return Pair.of(startParensCount, endParensCount);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		Pair <Integer,Integer> numOfParens = countParens(textArea);
		parensCount.setText(new LiteralText(" ( : "+numOfParens.getFirst()+" ) : "+numOfParens.getSecond()));

		if (keyCode == GLFW.GLFW_KEY_S && hasControlDown()) {
			save();
			return true;
		}
		if (keyCode==GLFW.GLFW_KEY_TAB) {
			textArea.write("  ");
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
	public void onClose() {
		if (SeventhSonMain.config.client.saveLispMachineOnGUIExit) {
			save();
		}
		this.client.openScreen(this.parent);
	}

	public void save() {
		blockEntity.text = textArea.getLines();
		ClientNetworkHandler.sendPacket(ClientNetworkHandler.sendTextStoringBlockData(blockEntity));
		Pair <Integer,Integer> numOfParens = countParens(textArea);
		parensCount.setText(new LiteralText(" ( : "+numOfParens.getFirst()+" ) : "+numOfParens.getSecond()));

	}

	@Override
    protected void init() {
        super.init();

        int height = this.height - 50;

		int textFieldWidth = (int) (width * (3.0 / 4.0));
		SpruceTextAreaWidget textArea;
		textArea = new SpruceTextAreaWidget(Position.of((width / 2) - (textFieldWidth / 2),
				0), textFieldWidth, height - 50,
				SeventhSonMain.getTranslatableText("screen", "lisp_machine", "insert_code_here"));
		if (this.textArea != null) {
			textArea.setText(this.textArea.getText());
		} else {
			if (this.blockEntity.text.isEmpty()) {
				textArea.setText("");
			} else {
				textArea.setLines(this.blockEntity.text);
			}
		}
		this.textArea = textArea;
		// Display as many lines as possible
		textArea.setCursorToStart();
		SpruceContainerWidget containerWidget = new SpruceContainerWidget(Position.of(this, 0, 50),
				width,
				height);

		int startX = width / 2 - 155;
		// Save and Evaluate button
		SpruceButtonWidget saveAndEval = new SpruceButtonWidget(Position.of(startX, height - 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "lisp_machine", "save_and_run"),
				button -> {
					save();
					ClientNetworkHandler.sendPacket(ClientNetworkHandler.activateSpell(blockEntity.getPos()));
				});
		// Save and Exit button
		SpruceButtonWidget saveAndExit = new SpruceButtonWidget(Position.of(startX + 160, height - 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "lisp_machine", "save_and_exit"),
				button -> {
					save();
					this.client.openScreen(this.parent);
				});

		SpruceButtonWidget pathSettings = new SpruceButtonWidget(Position.of(startX + 80, height - 29 - 20),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "lisp_machine", "path_settings"),
				button -> {
					this.client.openScreen(new PathSettingsScreen(blockEntity.getPos(), this));
				});


        parensCount=new SpruceLabelWidget(Position.of(this,0,20),
				new LiteralText("(= 0000 ,) = 0000 "),
				20,
				true);

		Pair <Integer,Integer> numOfParens = countParens(textArea);
		parensCount.setText(new LiteralText(" ( : "+numOfParens.getFirst()+" ) : "+numOfParens.getSecond()));

		containerWidget.addChild(textArea);
		containerWidget.addChild(saveAndEval);
		containerWidget.addChild(saveAndExit);
		containerWidget.addChild(pathSettings);
		this.addChild(containerWidget);
		this.addChild(parensCount);



	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

    @Override
    public void renderTitle(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        drawCenteredText(matrices, this.textRenderer, this.title, this.width / 2, 8, 16777215);
    }

	@Override
	public void setText(String[] text) {
		textArea.setLines(Arrays.asList(text));
	}
}
