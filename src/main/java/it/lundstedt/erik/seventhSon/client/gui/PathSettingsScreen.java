package it.lundstedt.erik.seventhSon.client.gui;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.client.BlockEntityFileRegistry;
import it.lundstedt.erik.seventhSon.client.util.ClientFileHelper;
import it.lundstedt.erik.seventhSon.util.helper.FileHelper;
import it.lundstedt.erik.seventhSon.util.helper.FileSelectHelper;
import me.lambdaurora.spruceui.Position;
import me.lambdaurora.spruceui.screen.SpruceScreen;
import me.lambdaurora.spruceui.widget.SpruceButtonWidget;
import me.lambdaurora.spruceui.widget.container.SpruceContainerWidget;
import me.lambdaurora.spruceui.widget.text.SpruceTextFieldWidget;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class PathSettingsScreen extends SpruceScreen {
	private final Screen parent;
    private SpruceTextFieldWidget pathField;
    private Path path;

    public BlockPos pos;

	public PathSettingsScreen(BlockPos pos, @Nullable Screen parent) {
		super(SeventhSonMain.getTranslatableText("screen", "path", "title"));
		this.parent = parent;
		this.pos = pos;
	}

	/*@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		if (keyCode == GLFW.GLFW_KEY_S && hasControlDown()) {
			save();
			return true;
		}
		if (keyCode==GLFW.GLFW_KEY_TAB)
		{
			textArea.write("  ");
		return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}*/

	@Override
	public void onClose() {
		this.client.openScreen(this.parent);
	}

	@Override
    protected void init() {
        super.init();

        int height = this.height - 50;

		int textFieldWidth = (int) (width * (3.0 / 4.0));
		SpruceTextFieldWidget pathField;
		pathField = new SpruceTextFieldWidget(Position.of((width / 2) - (textFieldWidth / 2),
				0), textFieldWidth - 75, 20,
				SeventhSonMain.getTranslatableText("screen", "path", "insert_path_here"));
		if (this.pathField != null) {
			pathField.setText(this.pathField.getText());
		} else {
			pathField.setText(BlockEntityFileRegistry.getInstance().getPathFromBlock(client, pos).map(FileHelper::writePathToString).orElse(""));
		}
		this.pathField = pathField;
		// Display as many lines as possible
		pathField.setCursorToStart();
		SpruceContainerWidget containerWidget = new SpruceContainerWidget(Position.of(this, 0, 50),
				width,
				height);

		int startX = width / 2 - 155;
		SpruceButtonWidget writeToFile = new SpruceButtonWidget(Position.of(startX, + 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "write_to_file"),
				button -> {
					if (resetPath()) {
						BlockEntityFileRegistry.getInstance().writeToFile(client, pos);
					}
				});
		SpruceButtonWidget readFromFile = new SpruceButtonWidget(Position.of(startX + 160, 0 + 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "read_from_file"),
				button -> {
					if (resetPath()) {
						BlockEntityFileRegistry.getInstance().readFromFile(client, pos, lines -> {
							if (parent instanceof Retextable) {
								((Retextable)parent).setText(lines);
							}
						});
					}
				});
		SpruceButtonWidget setPath = new SpruceButtonWidget(Position.of(startX + 160, 30 + 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "set_path"),
				button -> {
					resetPath();
				});
		SpruceButtonWidget openInEditor = new SpruceButtonWidget(Position.of(startX, 30 + 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "open_in_editor"),
				button -> {
					if (resetPath()) {
						BlockEntityFileRegistry.getInstance().getPathFromBlock(client, pos).ifPresent(ClientFileHelper::openTextFile);
					}
				});
		SpruceButtonWidget exit = new SpruceButtonWidget(Position.of(startX + 160, height - 29),
				150,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "exit"),
				button -> {
					onClose();
				});

		SpruceButtonWidget pathSetter = new SpruceButtonWidget(Position.of(pathField.getX() + pathField.getWidth(), pathField.getY()),
				75,
				20,
				SeventhSonMain.getTranslatableText("screen", "path", "path_chooser"),
				button -> {
					if (resetPath()) {
						File startingPath = BlockEntityFileRegistry.getInstance().getPathFromBlock(client, pos).map(Path::toFile)
								.orElse(client.runDirectory);
						FileSelectHelper.openLoadDialog(
								SeventhSonMain.getTranslatableText("screen", "path", "path_chooser.window").getString(),
								startingPath,
								SeventhSonMain.getTranslatableText("screen", "path", "path_chooser.description").getString(),
								"*").ifPresent(file -> {
									setPath(file.toPath());
						});
					}
				});

		containerWidget.addChild(pathField);
		containerWidget.addChild(writeToFile);
		containerWidget.addChild(readFromFile);
		containerWidget.addChild(setPath);
		containerWidget.addChild(openInEditor);
		containerWidget.addChild(pathSetter);
		containerWidget.addChild(exit);
		this.addChild(containerWidget);



	}

	private void setPath(Path path) {
		/*Path runDir = client.runDirectory.toPath().toAbsolutePath();
		SeventhSonLogger.info(path);
		SeventhSonLogger.info(runDir.subpath(0, runDir.getNameCount()-1));
		if (path.startsWith(runDir.subpath(0, runDir.getNameCount()-1))) {
			SeventhSonLogger.info("Test");
			path = path.subpath(runDir.getNameCount(), path.getNameCount());
		}*/ //TODO Make relative path if possible.
		BlockEntityFileRegistry.getInstance().addBlock(client, pos, path);
		pathField.setText(FileHelper.writePathToString(path));
	}

	private boolean resetPath() {
		String text = pathField.getText();
		try {
			path = Paths.get(text);
			BlockEntityFileRegistry.getInstance().addBlock(client, pos, path);
			return true;
		} catch (InvalidPathException e) {
			pathField.setText(FileHelper.writePathToString(path));
			client.getToastManager().add(SystemToast.create(client, SystemToast.Type.PACK_LOAD_FAILURE, SeventhSonMain.getTranslatableText("screen", "path", "invalid_path", text), SeventhSonMain.getTranslatableText("screen", "path", "please_specify_valid_path")));
			return false;
		}
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

    @Override
    public void renderTitle(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        drawCenteredText(matrices, this.textRenderer, this.title, this.width / 2, 8, 16777215);
    }

    public interface Retextable {
		void setText(String[] text);
    }

}
