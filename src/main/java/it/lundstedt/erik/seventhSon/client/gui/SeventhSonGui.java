package it.lundstedt.erik.seventhSon.client.gui;

import it.lundstedt.erik.seventhSon.common.screen_handler.SeventhSonScreenHandlers;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;

public class SeventhSonGui {

	public static void init() {
		ScreenRegistry.register(SeventhSonScreenHandlers.spellCasterHandler, SpellCaster::new);
	}

}
