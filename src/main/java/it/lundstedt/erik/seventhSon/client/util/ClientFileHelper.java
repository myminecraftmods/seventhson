package it.lundstedt.erik.seventhSon.client.util;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import net.minecraft.util.Util;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ClientFileHelper {

	public static void openTextFile(Path file) {
		if (SeventhSonMain.config.client.useCustomOpenTextFileCommand) {
			openFile(file, ClientFileHelper::getTextFileExectutor);
		} else {
			Util.getOperatingSystem().open(file.toFile());
		}
	}

	public static String[] getTextFileExectutor(Path file) {
		String[] array = SeventhSonMain.config.client.customOpenTextFileCommand.split(" ");
		List<String> list = new ArrayList<>(array.length+1);
		for (String command : array) {
			if (!command.isEmpty()) {
				list.add(command);
			}
		}

		list.add(file.toAbsolutePath().toString());

		return list.toArray(new String[]{});
	}

	public static void openFile(Path file, Function<Path, String[]> argumentGetter) {
		try {
			Process process = AccessController.doPrivileged((PrivilegedAction<Process>) () -> {
				try {
					return Runtime.getRuntime().exec(argumentGetter.apply(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			});

			if (process == null) return;

			for (String string : IOUtils.readLines(process.getErrorStream())) {
				SeventhSonLogger.error(string);
			}

			process.getInputStream().close();
			process.getErrorStream().close();
			process.getOutputStream().close();
		} catch (IOException var5) {
			SeventhSonLogger.error("Couldn't open file '" + file + "'");
			var5.printStackTrace();
		}
	}

}
