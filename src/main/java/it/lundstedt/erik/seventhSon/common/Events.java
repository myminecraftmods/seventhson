package it.lundstedt.erik.seventhSon.common;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.api.ExtraLispCodeCallback;
import it.lundstedt.erik.seventhSon.mixin.AccessorMinecraftServer;
import it.lundstedt.erik.seventhSon.network.ServerNetworkHandler;
import it.lundstedt.erik.seventhSon.util.ConfigCommons;
import it.lundstedt.erik.seventhSon.util.helper.FileHelper;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.fabricmc.fabric.api.event.player.UseEntityCallback;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.dynamic.RegistryOps;
import net.minecraft.util.registry.Registry;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Events {

	public static void init() {
		UseItemCallback.EVENT.register((player, world, hand) -> {
			ItemStack stack = player.getStackInHand(hand);

			if ((Items.WRITTEN_BOOK.equals(stack.getItem()) || Items.WRITABLE_BOOK.equals(stack.getItem())) && !player.isSneaking()) {
				if (LispHelper.parseLisp(player, stack)) {
					return TypedActionResult.success(stack, world.isClient());
				}
			}

			return TypedActionResult.pass(stack);
		});
		/*UseBlockCallback.EVENT.register((player, world, hand, hitResult) -> {
			if (player.isSneaking()) {
				return ActionResult.PASS;
			}
			if (LispHelper.parseLisp(player, world, hitResult.getBlockPos())) {
				return ActionResult.success(world.isClient());
			}
			return ActionResult.PASS;
		});

		UseEntityCallback.EVENT.register((player, world, hand, entity, hitResult) -> {
			if (player.isSneaking()) {
				return ActionResult.PASS;
			}
			if (LispHelper.parseLisp(player, entity)) {
				return ActionResult.success(world.isClient());
			}
			return ActionResult.PASS;
		});*/
		ServerTickEvents.START_SERVER_TICK.register(server -> {
			if (ConfigCommons.shouldSync) {
				ConfigCommons.shouldSync = false;
				ServerNetworkHandler.sendToAllExceptHost(server, ServerNetworkHandler.syncConfig());
			}
		});
		ServerLifecycleEvents.SERVER_STARTED.register(server -> {
			ConfigCommons.shouldSync = false;
		});
		String folderName = SeventhSonMain.MODID + "_scripts";
		ExtraLispCodeCallback.DEFAULT_CALLBACK.register((caster, object) -> {
			return LispHelper.loadLispFiles(caster, folderName + "/common");
		});
		ExtraLispCodeCallback.getEntityCallback().register((caster, object) -> {
			return LispHelper.loadLispFiles(caster, folderName + "/entities");
		});
		ExtraLispCodeCallback.getBlockCallback().register((caster, object) -> {
			return LispHelper.loadLispFiles(caster, folderName + "/blocks");
		});
		ExtraLispCodeCallback.getItemCallback().register((caster, object) -> {
			return LispHelper.loadLispFiles(caster, folderName + "/items");
		});
	}

}
