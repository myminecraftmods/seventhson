package it.lundstedt.erik.seventhSon.common.block_entities;

import it.lundstedt.erik.seventhSon.client.BlockEntityFileRegistry;
import it.lundstedt.erik.seventhSon.network.ClientNetworkHandler;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.math.BlockPos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LispMachineBlockEntity extends BlockEntity implements BlockEntityClientSerializable, BlockEntityFileRegistry.PathAble {

	public static final String TEXT = "Text";

	public List<String> text = new ArrayList<>();

	public LispMachineBlockEntity() {
		super(SeventhSonBlockEntities.textStoringBlock);
	}

	public void setText(List<String> text) {
		this.text = text;
		if (hasWorld() && !world.isClient()) {
			this.sync();
		}
	}

	@Override
	public void fromTag(BlockState state, CompoundTag tag) {
		super.fromTag(state, tag);
		fromClientTag(tag);
	}

	@Override
	public CompoundTag toTag(CompoundTag tag) {
		return toClientTag(super.toTag(tag));
	}


	@Override
	public void fromClientTag(CompoundTag tag) {
		text.clear();
		ListTag text = tag.getList(TEXT, StringTag.of("").getType());
		for (Tag line : text) {
			this.text.add(line.asString());
		}
	}

	@Override
	public CompoundTag toClientTag(CompoundTag tag) {
		ListTag text = new ListTag();
		for (String line : this.text) {
			text.add(StringTag.of(line));
		}
		tag.put(TEXT, text);
		return tag;
	}

	@Override
	public String[] getContents() {
		return text.toArray(new String[]{});
	}

	@Override
	public void putContents(String[] newContents) {
		text = new ArrayList<>(Arrays.asList(newContents));
		ClientNetworkHandler.sendPacket(ClientNetworkHandler.sendTextStoringBlockData(this));
	}
}
