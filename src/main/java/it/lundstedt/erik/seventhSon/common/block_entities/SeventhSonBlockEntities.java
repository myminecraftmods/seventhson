package it.lundstedt.erik.seventhSon.common.block_entities;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.blocks.SeventhSonBlocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.registry.Registry;

public class SeventhSonBlockEntities {

	public static BlockEntityType<LispMachineBlockEntity> textStoringBlock =
			register(
					"lisp_machine",
					BlockEntityType.Builder.create(LispMachineBlockEntity::new,
					SeventhSonBlocks.textStoringBlock).build(null)
			);

	public static void init() {

	}

	private static <T extends BlockEntity> BlockEntityType<T> register(String id, BlockEntityType<T> blockEntityType) {
		return Registry.register(Registry.BLOCK_ENTITY_TYPE, SeventhSonMain.getId(id), blockEntityType);
	}

}
