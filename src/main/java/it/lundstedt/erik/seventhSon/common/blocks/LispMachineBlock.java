package it.lundstedt.erik.seventhSon.common.blocks;

import it.lundstedt.erik.seventhSon.client.gui.LispMachineScreen;
import it.lundstedt.erik.seventhSon.common.block_entities.LispMachineBlockEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class LispMachineBlock extends BlockWithEntity {

	public LispMachineBlock(Settings settings) {
		super(settings);
	}

	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
		if (world.isClient()) {
			BlockEntity blockEntity = world.getBlockEntity(pos);
			if (blockEntity instanceof LispMachineBlockEntity) {
				openScreen((LispMachineBlockEntity) blockEntity);
				return ActionResult.success(true);
			}
			return ActionResult.FAIL;
		}
		return ActionResult.success(false);
	}

	@Nullable
	@Override
	public BlockEntity createBlockEntity(BlockView world) {
		return new LispMachineBlockEntity();
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Environment(EnvType.CLIENT)
	public void openScreen(LispMachineBlockEntity blockEntity) {
		MinecraftClient.getInstance().openScreen(new LispMachineScreen(blockEntity));




	}
}
