package it.lundstedt.erik.seventhSon.common.blocks;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.registry.Registry;

public class SeventhSonBlocks {

	public static Block textStoringBlock = register("lisp_machine",
			new LispMachineBlock(FabricBlockSettings.of(Material.STONE).strength(5, 5)),
			new Item.Settings().group(ItemGroup.MISC));

	public static void init() {

	}

	private static Block register(String id, Block block, Item.Settings settings) {
		Registry.register(Registry.ITEM, SeventhSonMain.getId(id),
		new BlockItem(block, settings));
		return registerNoItem(id, block);
	}

	private static Block registerNoItem(String id, Block block) {
		return Registry.register(Registry.BLOCK, SeventhSonMain.getId(id), block);
	}

}
