package it.lundstedt.erik.seventhSon.common.items;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import net.minecraft.item.*;
import net.minecraft.util.*;
import net.minecraft.util.registry.*;

public class SeventhSonItems {

	public static Item spellCasterBasic;
	
	public static Item testItem;
	
	
	
	
	static {
		spellCasterBasic = register("spell_caster_basic", new SpellCaster(new Item.Settings().group(ItemGroup.TOOLS).maxCount(1).rarity(Rarity.EPIC)));
		//testItem = register("testitem", new TestItem(new Item.Settings().group(ItemGroup.MISC).maxCount(1).rarity(Rarity.EPIC)));
	}


	public static void init() {

	}

	private static Item register(String id, Item item) {
        return Registry.register(Registry.ITEM, SeventhSonMain.getId(id), item);
    }

}