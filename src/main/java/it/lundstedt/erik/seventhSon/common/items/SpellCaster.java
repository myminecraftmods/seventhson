package it.lundstedt.erik.seventhSon.common.items;

import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SpellCaster extends Item {

	public SpellCaster(Settings settings) {
		super(settings);
	}
	
	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {

		ItemStack stack = user.getStackInHand(hand);

		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			if (user.isSneaking() && !world.isClient()) {
				user.openHandledScreen(component);
				return TypedActionResult.success(stack);
			} else if (!user.isSneaking()) {
				/*PlayerManaComponent playerComponent = PlayerManaComponent.getComponent(user).orElse(null);
				if (playerComponent != null && playerComponent.drainMana(component.getManaDrain())) {
					Type type = component.getType();
					return type.use(world, user, hand);
				}*/



/*
				ItemStack bookStack = component.getStack(0);
				if (LispHelper.parseLisp(user, bookStack)) {
					return TypedActionResult.success(stack, world.isClient());
				}
*/
				int actionSlotCount = component.actionSlotCount;

				for (int i = 0; i < 3; i++) {

				ItemStack bookStack = component.getStack(i);

				if (LispHelper.parseLisp(user, bookStack)) {
					return TypedActionResult.success(stack, world.isClient());
				}

				}



			}
		}
		return super.use(world, user, hand);
	}

	@Override
	public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.useOnEntity(stack, user, entity, hand);
		}
		return super.useOnEntity(stack, user, entity, hand);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(context.getStack()).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.useOnBlock(context);
		}
		return super.useOnBlock(context);
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.postHit(stack, target, attacker);
		}
		return super.postHit(stack, target, attacker);
	}

	@Override
	public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.finishUsing(stack, world, user);
		}
		return super.finishUsing(stack, world, user);
	}

	@Override
	public void usageTick(World world, LivingEntity user, ItemStack stack, int remainingUseTicks) {
		SpellCasterComponent.getComponent(stack).ifPresent(component -> {
			Type type = component.getType();
			type.usageTick(world, user, stack, remainingUseTicks);
		});
		super.usageTick(world, user, stack, remainingUseTicks);
	}

	@Override
	public int getMaxUseTime(ItemStack stack) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.getMaxUseTime(stack);
		}
		return super.getMaxUseTime(stack);
	}

	@Override
	public void onStoppedUsing(ItemStack stack, World world, LivingEntity user, int remainingUseTicks) {
		SpellCasterComponent.getComponent(stack).ifPresent(component -> {
			Type type = component.getType();
			type.onStoppedUsing(stack, world, user, remainingUseTicks);
		});
		super.onStoppedUsing(stack, world, user, remainingUseTicks);
	}

	@Override
	@Environment(EnvType.CLIENT)
	public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
		SpellCasterComponent.getComponent(stack).ifPresent(component -> {
			Type type = component.getType();
			type.appendTooltip(stack, world, tooltip, context);
		});
		super.appendTooltip(stack, world, tooltip, context);
	}

	@Override
	public Text getName(ItemStack stack) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.getName(stack);
		}
		return super.getName(stack);
	}

	@Override
	public boolean canMine(BlockState state, World world, BlockPos pos, PlayerEntity miner) { //TODO This assumes main hand...
		SpellCasterComponent component = SpellCasterComponent.getComponent(miner.getMainHandStack()).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.canMine(state, world, pos, miner);
		}
		return super.canMine(state, world, pos, miner);
	}

	@Override
	public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.getMiningSpeedMultiplier(stack, state);
		}
		return super.getMiningSpeedMultiplier(stack, state);
	}

	@Override
	public boolean postMine(ItemStack stack, World world, BlockState state, BlockPos pos, LivingEntity miner) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.postMine(stack, world, state, pos, miner);
		}
		return super.postMine(stack, world, state, pos, miner);
	}

	@Override
	public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
		SpellCasterComponent.getComponent(stack).ifPresent(component -> {
			Type type = component.getType();
			type.inventoryTick(stack, world, entity, slot, selected);
		});
		super.inventoryTick(stack, world, entity, slot, selected);
	}

	@Override
	public boolean isUsedOnRelease(ItemStack stack) {
		SpellCasterComponent component = SpellCasterComponent.getComponent(stack).orElse(null);
		if (component != null) {
			Type type = component.getType();
			return type.isUsedOnRelease(stack);
		}
		return super.isUsedOnRelease(stack);
	}
	
}
