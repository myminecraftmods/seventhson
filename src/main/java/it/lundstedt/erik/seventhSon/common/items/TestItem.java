package it.lundstedt.erik.seventhSon.common.items;

import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.minecraft.client.gui.screen.ingame.BookEditScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BookItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import org.armedbear.lisp.Interpreter;
import org.armedbear.lisp.LispObject;
import org.armedbear.lisp.scripting.AbclScriptEngine;
import org.armedbear.lisp.scripting.AbclScriptEngineFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptException;


public class TestItem extends Item {

	ScriptEngine se = new AbclScriptEngineFactory().getScriptEngine();


	public TestItem(Settings settings) {
		super(settings);
	}
	
	
	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand) {
		//freezes the game ...
		if (!world.isClient()) {
/*			Interpreter interpreter = LispHelper.getInterpreter();
			LispObject lispObject = interpreter.eval("(print \"hi\")");
			lispObject.execute();
			interpreter.kill(1);
*/
			try {
				se.eval("(print \"hello lisp\")");

				
				
				
			} catch (ScriptException e) {
				e.printStackTrace();
			}
			
			
			
		}
		
		return TypedActionResult.success(playerEntity.getStackInHand(hand));
	}
}
