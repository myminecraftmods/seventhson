package it.lundstedt.erik.seventhSon.common.registries;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.spell_components.action.*;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import net.fabricmc.fabric.api.event.registry.FabricRegistryBuilder;
import net.minecraft.util.registry.Registry;

public class ActionRegistry {

	public static Registry<ActionType> registry = FabricRegistryBuilder.createSimple(ActionType.class, SeventhSonMain.getId("action")).buildAndRegister();

	public static ActionType<ActionLightning> lightningProjectile = register("lightning_projectile", ActionLightning::new);
	public static ActionType<ActionCustomDrag> customDrag = register("custom_drag", ActionCustomDrag::new);
	public static ActionType<ActionProjectileActions> projectileActions = register("projectile_actions", ActionProjectileActions::new);
	public static ActionType<ActionTeleport> teleport = register("teleport", ActionTeleport::new);

	public static void init() {
		
	}
	
	private static <T extends Action> ActionType<T> register(String id, ActionType<T> action) {
		return Registry.register(registry, SeventhSonMain.getId(id), action);
	}

	@FunctionalInterface
	public interface ActionType<T extends Action> {
		T makeType(ActionType<T> type, Type from);
		default T createType(Type from) {
			return makeType(this, from);
		}
	}

}
