package it.lundstedt.erik.seventhSon.common.registries;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.util.InitSpells;
import it.lundstedt.erik.seventhSon.util.PlayerScriptContext;
import net.fabricmc.loader.entrypoint.minecraft.hooks.EntrypointUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import org.armedbear.lisp.LispObject;
import org.armedbear.lisp.Symbol;

import javax.script.Bindings;
import javax.script.SimpleBindings;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class SpellFunctionRegistry {

	public static Map<String, Function<PlayerScriptContext, Object>> spells = new HashMap<>();

	public static void init() {
		EntrypointUtils.invoke(SeventhSonMain.MODID, InitSpells.class, init -> init.initSpells(new InitSpells.RegistrationFunction() {
			@Override
			public void register(String name, Function<PlayerScriptContext, Object> object) {
				SpellFunctionRegistry.register(name, object);
			}

			@Override
			public void register(Symbol symbol) {
				SpellFunctionRegistry.register(symbol);
			}
		}));
	}

	public static void register(String name, Function<PlayerScriptContext, Object> object) {
		spells.put(name, object);
	}

	public static void register(Symbol symbol) {
		register(symbol.name.toString(), player -> symbol);
	}

}
