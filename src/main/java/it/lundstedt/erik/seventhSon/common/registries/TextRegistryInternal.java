package it.lundstedt.erik.seventhSon.common.registries;

import com.google.gson.JsonParseException;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.api.*;
import it.lundstedt.erik.seventhSon.common.block_entities.LispMachineBlockEntity;
import it.lundstedt.erik.seventhSon.mixin.AccessorSignBlockEntity;
import it.lundstedt.erik.seventhSon.util.BlockReference;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LecternBlockEntity;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.WritableBookItem;
import net.minecraft.item.WrittenBookItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.text.Text;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TextRegistryInternal implements TextRegistry {

	/*public static final Map<Predicate<ItemStack>, ItemText> itemRegistry = new HashMap<>();
	public static final Map<BiPredicate<World, BlockPos>, BlockText> blockRegistry = new HashMap<>();
	public static final Map<Predicate<Entity>, EntityText> entityRegistry = new HashMap<>();*/

	public static final Map<Class<?>, Entry<?>> registry = new HashMap<>();

	/*@Override
	public void registerItem(Predicate<ItemStack> validItem, ItemText itemText) {
		itemRegistry.put(validItem, itemText);
	}

	@Override
	public void registerBlock(BiPredicate<World, BlockPos> validBlock, BlockText blockText) {
		blockRegistry.put(validBlock, blockText);
	}

	@Override
	public void registerEntity(Predicate<Entity> validEntity, EntityText entityText) {
		entityRegistry.put(validEntity, entityText);
	}*/

	static {
		TextRegistry.getInstance().registerType(BlockReference.class);
		TextRegistry.getInstance().registerType(Entity.class);
		TextRegistry.getInstance().registerType(ItemStack.class);
	}

	public static void init() {
		TextRegistry.getInstance().registerItem(item -> item.getItem() instanceof WritableBookItem, new TextGetter<ItemStack>() {
			@Override
			public String[] getText(ItemStack item) {
				return readVanillaBook(item);
			}

			@Override
			public double getManaFactor(ItemStack item) {
				return 0.8;
			}
		});
		//Less mana
		TextRegistry.getInstance().registerItem(item -> item.getItem() instanceof WrittenBookItem, TextRegistryInternal::readVanillaBook);
		TextRegistry.getInstance().registerBlock(blockReference -> {
			BlockEntity tile = blockReference.getBlockEntity();
			return tile instanceof LecternBlockEntity && ((LecternBlockEntity) tile).hasBook();
		}, new TextGetter<BlockReference>() {
			@Override
			public String[] getText(BlockReference blockReference) {
				LecternBlockEntity block = (LecternBlockEntity) blockReference.getBlockEntity();
				ItemStack book = block.getBook();
				return TextRegistry.getInstance().get(book).map(text -> text.getText(book)).orElse(new String[]{});
			}

			@Override
			public double getManaFactor(BlockReference blockReference) {
				LecternBlockEntity block = (LecternBlockEntity) blockReference.getBlockEntity();
				ItemStack book = block.getBook();
				return TextRegistry.getInstance().get(book).map(text -> text.getManaFactor(book) * 0.8).orElse(Double.MAX_VALUE);
			}
		});
		TextRegistry.getInstance().registerBlock(blockReference -> {
			BlockEntity tile = blockReference.getBlockEntity();
			return tile instanceof SignBlockEntity;
		}, new TextGetter<BlockReference>() {
			@Override
			public String[] getText(BlockReference blockReference) {
				AccessorSignBlockEntity block = (AccessorSignBlockEntity) blockReference.getBlockEntity();
				return Arrays.stream(block.getText()).map(Text::getString).collect(Collectors.toList()).toArray(new String[]{});
			}

			@Override
			public double getManaFactor(BlockReference blockReference) {
				return 0.25;
			}
		});
		TextRegistry.getInstance().registerBlock(blockReference -> {
			BlockEntity tile = blockReference.getBlockEntity();
			return tile instanceof LispMachineBlockEntity;
		}, new TextGetter<BlockReference>() {
			@Override
			public String[] getText(BlockReference blockReference) {
				LispMachineBlockEntity block = (LispMachineBlockEntity) blockReference.getBlockEntity();

				String[] text = new String[block.text.size()+1];
				text[0] = LispHelper.headerStart + " " + (SeventhSonMain.config.lispHeaders.size() > 0 ? SeventhSonMain.config.lispHeaders.get(0) : "");
				for (int i = 1; i < text.length; i++) {
					text[i] = block.text.get(i-1);
				}
				return text;
			}

			@Override
			public double getManaFactor(BlockReference blockReference) {
				return 1;
			}
		});
	}

	public static String[] readVanillaBook(ItemStack book) {
		CompoundTag tag = book.getTag();
		if (tag == null) {
			return new String[]{};
		} else {
			List<String> pages = new ArrayList<>();
			ListTag listTag = tag.getList("pages", 8);

			for(int j = 0; j < listTag.size(); ++j) {
				String line = listTag.getString(j);
				if (line.startsWith("{")) {
					try {
						line = Text.Serializer.fromJson(line).getString();
					} catch (JsonParseException | NullPointerException ignored) {}
				}
				pages.add(line);
			}
			return pages.toArray(new String[]{});
		}
	}

	/*@Override
	public Optional<ItemText> get(ItemStack item) {
		if (item == null) return Optional.empty();
		for (Map.Entry<Predicate<ItemStack>, ItemText> entry : itemRegistry.entrySet()) {
			if (entry.getKey().test(item)) {
				ItemText text = entry.getValue();
				String[] contents = text.getText(item);
				if (LispHelper.hasValidHeader(contents)) {
					return Optional.of(text);
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<BlockText> get(World world, BlockPos pos) {
		for (Map.Entry<BiPredicate<World, BlockPos>, BlockText> entry : blockRegistry.entrySet()) {
			if (entry.getKey().test(world, pos)) {
				BlockText text = entry.getValue();
				String[] contents = text.getText(world, pos);
				if (LispHelper.hasValidHeader(contents)) {
					return Optional.of(text);
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<EntityText> get(Entity entity) {
		if (entity == null) return Optional.empty();
		for (Map.Entry<Predicate<Entity>, EntityText> entry : entityRegistry.entrySet()) {
			if (entry.getKey().test(entity)) {
				EntityText text = entry.getValue();
				String[] contents = text.getText(entity);
				if (LispHelper.hasValidHeader(contents)) {
					return Optional.of(text);
				}
			}
		}
		return Optional.empty();
	}*/

	@Override
	public <T> void register(Class<T> type, Predicate<T> valid, TextGetter<T> getter) {
		if (!registry.containsKey(type)) {
			throw new IllegalArgumentException(type + " has not been registered!");
		}
		((Map<Predicate<T>, TextGetter<T>>) (Object) registry.get(type).registry).put(valid, getter);
	}

	@Override
	public <T> void registerType(Class<T> type, Entry<T> entry) {
		registry.put(type, entry);
	}

	@Override
	public <T> Optional<TextGetter<T>> get(Class<T> objectClass, T object) {
		Map<Predicate<T>, TextGetter<T>> map = (Map<Predicate<T>, TextGetter<T>>) (Object) registry.get(objectClass).registry;
		for (Map.Entry<Predicate<T>, TextGetter<T>> entry : map.entrySet()) {
			if (entry.getKey().test(object)) {
				TextGetter<T> getter = entry.getValue();
				String[] contents = getter.getText(object);
				if (LispHelper.hasValidHeader(contents)) {
					return Optional.of(getter);
				}
			}
		}
		return Optional.empty();
	}

	public static class Entry<T> {
		public Map<Predicate<T>, TextGetter<T>> registry = new HashMap<>();
		public Event<ExtraLispCodeCallback<T>> callback = EventFactory.createArrayBacked(ExtraLispCodeCallback.class, listeners -> (caster, object) -> {
			List<String> list = new ArrayList<>();
			for (ExtraLispCodeCallback<T> callback : listeners) {
				list.addAll(Arrays.asList(callback.getAdditionalCode(caster, object)));
			}
			return list.toArray(new String[]{});
		});
	}
}
