package it.lundstedt.erik.seventhSon.common.registries;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.spell_components.type.DummyType;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.common.spell_components.type.TypeShootFireball;
import it.lundstedt.erik.seventhSon.common.spell_components.type.TypeShootWitherSkull;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import net.fabricmc.fabric.api.event.registry.FabricRegistryBuilder;
import net.minecraft.util.registry.Registry;

public class TypeRegistry {

	public static Registry<TypeType> registry = FabricRegistryBuilder.createDefaulted(TypeType.class, SeventhSonMain.getId("type"), SeventhSonMain.getId("dummy")).buildAndRegister();

	public static TypeType<DummyType> dummy = register("dummy", DummyType::new);
	public static TypeType<TypeShootFireball> shootFireball = register("shoot_fireball", TypeShootFireball::new);
	public static TypeType<TypeShootWitherSkull> shootWitherSkull = register("shoot_wither_skull", TypeShootWitherSkull::new);

	public static void init() {
		
	}
	
	private static <T extends Type> TypeType<T> register(String id, TypeType<T> type) {
		return Registry.register(registry, SeventhSonMain.getId(id), type);
	}

	@FunctionalInterface
	public interface TypeType<T extends Type> {
		T makeType(TypeType<T> type, SpellCasterComponent component);

		default T createType(SpellCasterComponent component) {
			return this.makeType(this, component);
		}
	}

}
