package it.lundstedt.erik.seventhSon.common.screen_handler;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.items.SeventhSonItems;
import it.lundstedt.erik.seventhSon.component.ComponentInitializer;
import it.lundstedt.erik.seventhSon.mixin.AccessorSlot;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class SeventhSonScreenHandlers {

	public static final ScreenHandlerType<SpellCasterBasicHandler> spellCasterHandler = ScreenHandlerRegistry.registerExtended(SeventhSonMain.getId("spell-caster"), (id, player, buf) -> {
		int slot = buf.readInt();
		ItemStack stack = player.getStack(slot);
		if (stack.getItem().equals(SeventhSonItems.spellCasterBasic)) {
			return new SpellCasterBasicHandler(id, player, ComponentInitializer.SPELL_CASTER.get(stack));
		}
		return null;
	});


	public static final ScreenHandlerType<TestItemBasicHandler> testItemHandler = ScreenHandlerRegistry.registerExtended(SeventhSonMain.getId("test-item"), (id, player, buf) -> {
		int slot = buf.readInt();
		ItemStack stack = player.getStack(slot);
		if (stack.getItem().equals(SeventhSonItems.testItem)) {
			TestItemBasicHandler testItemBasicHandler = new TestItemBasicHandler(id, player,null);
			return testItemBasicHandler;
		}
		return null;
	});



	public static void init() {
	
	}

	public static void fixSlotsForSpellCaster(List<Slot> slots, Consumer<Slot> slotAdder) {
		List<Slot> newSlots = new ArrayList<>();
		for (Slot slot : slots) {
			newSlots.add(new Slot(slot.inventory, ((AccessorSlot)slot).getIndex(), slot.x, slot.y) {

				@Override
				public boolean canInsert(ItemStack stack) {
					return true;
				}

				@Override
				public boolean canTakeItems(PlayerEntity playerEntity) {
					return !getStack().getItem().equals(SeventhSonItems.spellCasterBasic);
				}

			});
		}

		slots.clear();
		for (Slot slot : newSlots) {
			slotAdder.accept(slot);
		}
	}

}
