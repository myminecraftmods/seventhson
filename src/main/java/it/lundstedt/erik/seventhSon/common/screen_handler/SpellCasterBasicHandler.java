package it.lundstedt.erik.seventhSon.common.screen_handler;

import it.lundstedt.erik.seventhSon.common.items.SeventhSonItems;
import it.lundstedt.erik.seventhSon.mixin.AccessorSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.Generic3x3ContainerScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;

import java.util.ArrayList;
import java.util.List;

public class SpellCasterBasicHandler extends Generic3x3ContainerScreenHandler {

	public SpellCasterBasicHandler(int syncId, PlayerInventory playerInventory) {
		super(syncId, playerInventory);
	}

	public SpellCasterBasicHandler(int syncId, PlayerInventory playerInventory, Inventory inv) {
		super(syncId, playerInventory, inv);
		SeventhSonScreenHandlers.fixSlotsForSpellCaster(this.slots, this::addSlot);
	}


	@Override
	public ScreenHandlerType<?> getType() {
		return SeventhSonScreenHandlers.spellCasterHandler;
	}
}
