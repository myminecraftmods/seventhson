package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public abstract class Action {

	public ActionRegistry.ActionType<?> actionType;

	/**
	 * This will exist when this action was triggered by a spell directly.
	 * However, it might not exist when attached to for example a projectile.
	 */
	public Optional<Type> type;
	public int magnitude = 0;
	public Collection<ItemStack> modifiers;

	public Action(ActionRegistry.ActionType<?> actionType, Type type) {
		this.actionType = actionType;
		this.type = Optional.ofNullable(type);
		if (type != null) {
			this.modifiers = type.getModifiers();
		}
	}

	public void affectBlock(World world, BlockPos pos, Entity user) {

	}

	public void affectEntity(Entity entity, Entity user) {

	}

	public void affectAt(World world, Vec3d pos, Entity user) {

	}

	public abstract ItemPredicates createdFrom();

	public ActionRegistry.ActionType<?> getType() {
		return actionType;
	}

	public abstract double getManaDrain();

	public int getModifierMagnitude(ItemPredicates predicate) {
		int magnitude = 0;
		for (ItemStack stack : modifiers) {
			if (predicate.test(stack)) {
				magnitude += stack.getCount();
			}
		}
		return magnitude;
	}

	public Collection<ItemStack> getModifiers(ItemPredicates predicate) {
		return getModifiers(predicate, true);
	}

	public Collection<ItemStack> getModifiers(ItemPredicates predicate, boolean hasModifier) {
		Collection<ItemStack> modifiers = new ArrayList<>();
		for (ItemStack stack : this.modifiers) {
			if (hasModifier == predicate.test(stack)) {
				modifiers.add(stack);
			}
		}
		return modifiers;
	}

	public boolean isUnlimited() {
		return hasModifier(Type.limitBreaker);
	}

	public boolean hasModifier(ItemPredicates predicate) {
		return checkForModifier(predicate, true);
	}

	public boolean lacksModifier(ItemPredicates predicate) {
		return checkForModifier(predicate, false);
	}

	public boolean checkForModifier(ItemPredicates predicate, boolean hasModifier) {
		for (ItemStack stack : modifiers) {
			if (predicate.test(stack)) {
				return hasModifier;
			}
		}
		return !hasModifier;
	}

}
