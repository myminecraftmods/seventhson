package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.DragChanger;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.entity.projectile.ExplosiveProjectileEntity;
import net.minecraft.item.Items;

public class ActionCustomDrag extends BaseActionProjectile<ExplosiveProjectileEntity> implements DragChanger {

	public ActionCustomDrag(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.SUGAR).build());
	}

	@Override
	public double getManaDrain() {
		return 25 + 10 * magnitude;
	}

	@Override
	public double getDrag(double defaultDrag, double currentDrag) {
		return currentDrag + magnitude/5d;
	}
}
