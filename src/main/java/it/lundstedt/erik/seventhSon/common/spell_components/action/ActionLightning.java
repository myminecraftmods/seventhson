package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.predicate.NumberRange;
import net.minecraft.predicate.item.EnchantmentPredicate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ActionLightning extends Action {

	public ActionLightning(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(
				ItemPredicateHelper.createBuilder().enchantment(new EnchantmentPredicate(Enchantments.CHANNELING, NumberRange.IntRange.ANY)).build(),
				ItemPredicateHelper.createBuilder().storedEnchantment(new EnchantmentPredicate(Enchantments.CHANNELING, NumberRange.IntRange.ANY)).build()
		);
	}

	@Override
	public double getManaDrain() {
		return 100;
	}

	@Override
	public void affectAt(World world, Vec3d pos, Entity user) {
		if (!world.isClient()) {
			LightningEntity lightning = EntityType.LIGHTNING_BOLT.create(world);
			lightning.updatePosition(pos.getX(), pos.getY(), pos.getZ());
			lightning.setChanneler(user instanceof ServerPlayerEntity ? (ServerPlayerEntity)user : null);
			lightning.setCosmetic(false);
			world.spawnEntity(lightning);
		}
	}
}
