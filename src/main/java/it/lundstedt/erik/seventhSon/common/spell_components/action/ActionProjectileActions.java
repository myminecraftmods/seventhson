package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.SaveableAction;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.Collection;

public class ActionProjectileActions extends BaseActionProjectile<ProjectileEntity> {

	public static final String ACTIONS = "Actions";
	public static final String ID = "ActionId";

	public Collection<Action> actions = new ArrayList<>();

	public ActionProjectileActions(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public ItemPredicates createdFrom() {
		return ItemPredicates.NONE;
	}

	@Override
	public double getManaDrain() {
		return 0;
	}

	@Override
	public void affectBlock(World world, BlockPos pos, Entity user) {
		for (Action action : actions) {
			action.affectBlock(world, pos, user);
		}
	}

	@Override
	public void affectEntity(Entity entity, Entity user) {
		for (Action action : actions) {
			action.affectEntity(entity, user);
		}
	}

	@Override
	public void affectAt(World world, Vec3d pos, Entity user) {
		for (Action action : actions) {
			action.affectAt(world, pos, user);
		}
	}

	@Override
	public void readFromNbt(CompoundTag tag) {
		this.actions.clear();
		SaveableAction.readDefaults(this, tag);
		ListTag actions = tag.getList(ACTIONS, new CompoundTag().getType());
		for (Tag actionTag : actions) {
			if (actionTag instanceof CompoundTag) {
				CompoundTag actionNbt = (CompoundTag) actionTag;
				ActionRegistry.registry.getOrEmpty(new Identifier(actionNbt.getString(ID))).ifPresent(type -> {
					Action action = type.createType(null);
					if (action instanceof SaveableAction) {
						((SaveableAction)action).readFromNbt(actionNbt);
					}
					this.actions.add(action);
				});
			}
		}
	}

	@Override
	public CompoundTag writeToNbt(CompoundTag tag) {
		SaveableAction.writeDefaults(this, tag);
		ListTag actions = new ListTag();
		for (Action action : this.actions) {
			CompoundTag actionTag = new CompoundTag();
			Identifier id = ActionRegistry.registry.getId(action.getType());
			if (id != null) {
				actionTag.putString(ID, id.toString());
			}
			if (action instanceof SaveableAction) {
				((SaveableAction)action).writeToNbt(actionTag);
			}
			actions.add(actionTag);
		}
		tag.put(ACTIONS, actions);
		return tag;
	}
}
