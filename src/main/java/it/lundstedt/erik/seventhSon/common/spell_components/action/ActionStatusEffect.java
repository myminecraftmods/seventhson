package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;

public class ActionStatusEffect extends Action {
	public ActionStatusEffect(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public ItemPredicates createdFrom() {
		return null;
	}

	@Override
	public double getManaDrain() {
		return 10;
	}
}
