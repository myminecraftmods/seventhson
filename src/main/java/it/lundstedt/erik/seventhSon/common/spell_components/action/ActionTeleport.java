package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import it.lundstedt.erik.seventhSon.util.helper.TeleportHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.TeleportTarget;

public class ActionTeleport extends BaseActionProjectile<ProjectileEntity> {

	public static ItemPredicates swapModifier = new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.SNOWBALL).build());

	public ActionTeleport(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public boolean onCollision(HitResult hitResult) {
		if (!projectile.world.isClient() && hitResult.getType() != HitResult.Type.MISS) {
			Entity owner = projectile.getOwner();
			if (owner != null) {
				Vec3d newPos = hitResult.getPos();
				Vec3d newVelocity = Vec3d.ZERO;
				float newYaw = owner.yaw;
				float newPitch = owner.pitch;
				if (hitResult.getType().equals(HitResult.Type.ENTITY) && this.hasModifier(swapModifier)) {
					Entity target = ((EntityHitResult)hitResult).getEntity();
					newPos = target.getPos();
					newVelocity = target.getVelocity();
					newYaw = target.yaw;
					newPitch = target.pitch;
					TeleportHelper.teleport(target, (ServerWorld) owner.world, new TeleportTarget(owner.getPos(), owner.getVelocity(), owner.yaw, owner.pitch));
				}
				owner = TeleportHelper.teleport(owner, (ServerWorld) projectile.world, new TeleportTarget(newPos, newVelocity, newYaw, newPitch));
			}
		}
		return true;
	}

	@Override
	public double getManaDrain() {
		return 50;
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.ENDER_PEARL).build());
	}
}
