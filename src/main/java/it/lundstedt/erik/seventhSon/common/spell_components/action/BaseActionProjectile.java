package it.lundstedt.erik.seventhSon.common.spell_components.action;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.ProjectileAttachable;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import net.minecraft.entity.projectile.ProjectileEntity;

public abstract class BaseActionProjectile<T extends ProjectileEntity> extends Action implements ProjectileAttachable<T> {

	public T projectile;

	public BaseActionProjectile(ActionRegistry.ActionType<?> actionType, Type type) {
		super(actionType, type);
	}

	@Override
	public T getProjectile() {
		return projectile;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean setProjectile(ProjectileEntity projectile) {
		try {
			this.projectile = (T) projectile;
			return true;
		} catch (ClassCastException ignored) {
			return false;
		}
	}


}
