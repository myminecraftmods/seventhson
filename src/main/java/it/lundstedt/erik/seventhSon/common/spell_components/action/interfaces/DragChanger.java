package it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces;

public interface DragChanger {

	double getDrag(double defaultDrag, double currentDrag);

}
