package it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces;

import it.lundstedt.erik.seventhSon.common.spell_components.action.Action;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;

public interface ProjectileAttachable<T extends ProjectileEntity> extends SaveableAction, TickableAction {

	/*
	 * If false it won't hit.
	 * @param hitResult
	 * @return
	 */
	default boolean onCollision(HitResult hitResult) {
		if (hitResult.getType() != HitResult.Type.MISS && this instanceof Action) {
			ProjectileEntity projectile = getProjectile();
			((Action)this).affectAt(projectile.world, hitResult.getPos(), projectile.getOwner());
		}
		return true;
	}

	default boolean onEntityHit(EntityHitResult entityHitResult) {
		if (this instanceof Action) {
			ProjectileEntity projectile = getProjectile();
			((Action)this).affectEntity(entityHitResult.getEntity(), projectile.getOwner());
		}
		return true;
	}

	default boolean onBlockHit(BlockHitResult blockHitResult) {
		if (this instanceof Action) {
			ProjectileEntity projectile = getProjectile();
			((Action)this).affectBlock(projectile.world, blockHitResult.getBlockPos(), projectile.getOwner());
		}
		return true;
	}

	T getProjectile();

	boolean setProjectile(ProjectileEntity projectile);
}
