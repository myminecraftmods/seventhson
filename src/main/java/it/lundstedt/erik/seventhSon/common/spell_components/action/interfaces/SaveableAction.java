package it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces;

import it.lundstedt.erik.seventhSon.common.spell_components.action.Action;
import net.minecraft.nbt.CompoundTag;

/**
 * Warning, this will probably not save the type!
 */
public interface SaveableAction {

	public static final String MAGNITUDE = "Magnitude";

	default void readFromNbt(CompoundTag tag) {
		if (this instanceof Action) {
			readDefaults(this, tag);
		}
	}

	default CompoundTag writeToNbt(CompoundTag tag) {
		writeDefaults(this, tag);
		return tag;
	}

	static void writeDefaults(SaveableAction action, CompoundTag tag) {
		if (action instanceof Action) {
			tag.putInt(MAGNITUDE, ((Action)action).magnitude);
		}
	}

	static void readDefaults(SaveableAction action, CompoundTag tag) {
		if (action instanceof Action) {
			((Action)action).magnitude = tag.getInt(MAGNITUDE);
		}
	}

}
