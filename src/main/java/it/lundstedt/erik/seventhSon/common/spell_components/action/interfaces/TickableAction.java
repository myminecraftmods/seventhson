package it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces;

public interface TickableAction {

	default void tick() {

	}

}
