package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public abstract class BaseTypeProjectile extends Type {
	public BaseTypeProjectile(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		super(type, component);
	}

	public boolean setPos() {
		return true;
	}

	public abstract ProjectileEntity prepareProjectile(World world, PlayerEntity user, Hand hand);

	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		if (!world.isClient()) {
			ProjectileEntity projectile = prepareProjectile(world, user, hand);
			if (setPos()) {
				projectile.updatePosition(user.getX(), user.getEyeY(), user.getZ());
			}
			addActionsToProjectile(projectile);
			world.spawnEntity(projectile);
		}
		return TypedActionResult.success(user.getStackInHand(hand));
	}

}
