package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.block.Blocks;

/**
 * Used instead of null to avoid nullpointers.
 */
public class DummyType extends Type {

	public DummyType(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		super(type, component);
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(ItemPredicateHelper.createBuilder().item(Blocks.BEDROCK).build());
	}

	@Override
	public double getManaDrain() {
		return 0;
	}
}
