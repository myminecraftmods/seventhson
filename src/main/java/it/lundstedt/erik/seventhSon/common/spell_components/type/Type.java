package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.action.Action;
import it.lundstedt.erik.seventhSon.common.spell_components.action.ActionProjectileActions;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.ProjectileAttachable;
import it.lundstedt.erik.seventhSon.component.ProjectileComponent;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.Items;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class Type {

	public TypeRegistry.TypeType<?> type;
	public SpellCasterComponent component;

	public static ItemPredicates limitBreaker = new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.NETHER_STAR).build());

	public Type(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		this.type = type;
		this.component = component;
	}

	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		return TypedActionResult.pass(user.getStackInHand(hand));
	}

	public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
		return ActionResult.PASS;
	}

	public ActionResult useOnBlock(ItemUsageContext context) {
		return ActionResult.PASS;
	}

	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		return false;
	}

	public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {
		return stack;
	}

	public void usageTick(World world, LivingEntity user, ItemStack stack, int remainingUseTicks) {

	}

	public int getMaxUseTime(ItemStack stack) {
		return 0;
	}

	public void onStoppedUsing(ItemStack stack, World world, LivingEntity user, int remainingUseTicks) {

	}

	@Environment(EnvType.CLIENT)
	public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {

	}

	public Text getName(ItemStack stack) {
		return new TranslatableText(stack.getItem().getTranslationKey(stack));
	}

	public boolean canMine(BlockState state, World world, BlockPos pos, PlayerEntity miner) {
		return true;
	}

	public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
		return 1;
	}

	public boolean postMine(ItemStack stack, World world, BlockState state, BlockPos pos, LivingEntity miner) {
		return true;
	}

	public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {

	}

	public boolean isUsedOnRelease(ItemStack stack) {
		return false;
	}

	public abstract ItemPredicates createdFrom();

	public TypeRegistry.TypeType<?> getType() {
		return type;
	}

	public Collection<Action> getActions() {
		return component.getActions(this);
	}

	public Collection<ItemStack> getModifiers() {
		return component.getModifiers();
	}

	public int getModifierMagnitude(ItemPredicates predicate) {
		int magnitude = 0;
		for (ItemStack stack : getModifiers()) {
			if (predicate.test(stack)) {
				magnitude += stack.getCount();
			}
		}
		return magnitude;
	}

	public Collection<ItemStack> getModifiers(ItemPredicates predicate) {
		return getModifiers(predicate, true);
	}

	public Collection<ItemStack> getModifiers(ItemPredicates predicate, boolean hasModifier) {
		Collection<ItemStack> modifiers = new ArrayList<>();
		for (ItemStack stack : getModifiers()) {
			if (hasModifier == predicate.test(stack)) {
				modifiers.add(stack);
			}
		}
		return modifiers;
	}

	public boolean isUnlimited() {
		return hasModifier(limitBreaker);
	}

	public boolean hasModifier(ItemPredicates predicate) {
		return checkForModifier(predicate, true);
	}

	public boolean lacksModifier(ItemPredicates predicate) {
		return checkForModifier(predicate, false);
	}

	public boolean checkForModifier(ItemPredicates predicate, boolean hasModifier) {
		for (ItemStack stack : getModifiers()) {
			if (predicate.test(stack)) {
				return hasModifier;
			}
		}
		return !hasModifier;
	}

	public void addActionsToProjectile(ProjectileEntity projectile) {
		ProjectileComponent.getComponent(projectile).ifPresent(component -> {
			List<Action> others = new ArrayList<>();
			for (Action action : getActions()) {
				if (action instanceof ProjectileAttachable<?>) {
					if (((ProjectileAttachable<?>)action).setProjectile(projectile)) {
						component.addAction((ProjectileAttachable<?>)action);
					}
				} else {
					others.add(action);
				}
			}
			ActionProjectileActions action = ActionRegistry.projectileActions.createType(this);
			action.setProjectile(projectile);
			action.actions.addAll(others);
			component.addAction(action);
		});
	}

	public abstract double getManaDrain();

}
