package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;

public class TypeAreaEffect extends Type {

	public TypeAreaEffect(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		super(type, component);
	}

	@Override
	public ItemPredicates createdFrom() {
		return null;
	}

	@Override
	public double getManaDrain() {
		return 0;
	}
}
