package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class TypeShootFireball extends BaseTypeProjectile {

	public ItemPredicates explodes = new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.GUNPOWDER).build());
	public ItemPredicates bigExplodes = new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.TNT).build());

	public TypeShootFireball(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		super(type, component);
	}

	@Override
	public ProjectileEntity prepareProjectile(World world, PlayerEntity user, Hand hand) {
		Vec3d facing = user.getRotationVector();
		int magnitude = getModifierMagnitude(explodes) + getModifierMagnitude(bigExplodes) * 5;
		if (magnitude > 0) {
			FireballEntity fireballEntity = new FireballEntity(world, user, facing.getX(), facing.getY(), facing.getZ());
			fireballEntity.explosionPower = MathHelper.clamp(magnitude/5, 1, isUnlimited() ? Integer.MAX_VALUE : 8);
			return fireballEntity;
		} else {
			return new SmallFireballEntity(world, user, facing.getX(), facing.getY(), facing.getZ());
		}
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.FIRE_CHARGE).build());
	}

	@Override
	public double getManaDrain() {
		int magnitude = getModifierMagnitude(explodes) + getModifierMagnitude(bigExplodes) * 5;
		if (magnitude > 0) {
			return 25 + 10 * magnitude;
		}
		return 25;
	}


}
