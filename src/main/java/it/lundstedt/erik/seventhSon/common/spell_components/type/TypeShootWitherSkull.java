package it.lundstedt.erik.seventhSon.common.spell_components.type;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.component.SpellCasterComponent;
import it.lundstedt.erik.seventhSon.util.helper.ItemPredicateHelper;
import it.lundstedt.erik.seventhSon.util.ItemPredicates;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.WitherSkullEntity;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class TypeShootWitherSkull extends BaseTypeProjectile {

	public TypeShootWitherSkull(TypeRegistry.TypeType<?> type, SpellCasterComponent component) {
		super(type, component);
	}

	@Override
	public ProjectileEntity prepareProjectile(World world, PlayerEntity user, Hand hand) {
		Vec3d facing = user.getRotationVector();
		WitherSkullEntity fireballEntity = new WitherSkullEntity(world, user, facing.getX(), facing.getY(), facing.getZ());
		//fireballEntity.setCharged(true);
		return fireballEntity;
	}

	@Override
	public ItemPredicates createdFrom() {
		return new ItemPredicates(ItemPredicateHelper.createBuilder().item(Items.WITHER_SKELETON_SKULL).build());
	}

	@Override
	public double getManaDrain() {
		return 25;
	}
}
