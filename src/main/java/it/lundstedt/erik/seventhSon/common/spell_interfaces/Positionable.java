package it.lundstedt.erik.seventhSon.common.spell_interfaces;

import it.lundstedt.erik.seventhSon.common.spell_objects.LispBlockPos;

public interface Positionable {

	LispBlockPos getBlockPos();

}
