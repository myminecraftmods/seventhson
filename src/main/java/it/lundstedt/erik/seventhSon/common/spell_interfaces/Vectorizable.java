package it.lundstedt.erik.seventhSon.common.spell_interfaces;

import it.lundstedt.erik.seventhSon.common.spell_objects.LispVec3d;

public interface Vectorizable {

	LispVec3d getVec3d();

}
