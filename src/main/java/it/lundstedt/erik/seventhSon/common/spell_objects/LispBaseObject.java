package it.lundstedt.erik.seventhSon.common.spell_objects;

import net.minecraft.entity.Entity;
import org.armedbear.lisp.LispError;
import org.armedbear.lisp.LispObject;

import static org.armedbear.lisp.Lisp.error;

public class LispBaseObject<T> extends LispObject {

	public T innerObject;

	public LispBaseObject(T innerObject) {
		this.innerObject = innerObject;
	}

	@Override
	public T javaInstance() {
		return innerObject;
	}

	@Override
	public Object javaInstance(Class<?> c) {
		if (c.isAssignableFrom(innerObject.getClass())) {
			return c.cast(innerObject);
		}

		return error(new LispError("The value " + princToString() + " is not of class " + c.getName()));
	}

	@Override
	public boolean eql(LispObject obj) {
		if (obj instanceof LispBaseObject) {
			return ((LispBaseObject<?>)obj).innerObject == innerObject;
		}
		return false;
	}

	@Override
	public boolean equal(LispObject obj) {
		if (obj instanceof LispBaseObject) {
			LispBaseObject<?> other = (LispBaseObject<?>) obj;
			return this.innerObject.equals(other.innerObject);
		}
		return false;
	}

	@Override
	public boolean equalp(LispObject obj) {
		return equal(obj);
	}

	@Override
	public String toString() {
		return innerObject.toString();
	}

}
