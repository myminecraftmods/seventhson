package it.lundstedt.erik.seventhSon.common.spell_objects;

import it.lundstedt.erik.seventhSon.common.spell_interfaces.Positionable;
import it.lundstedt.erik.seventhSon.common.spell_interfaces.Vectorizable;
import net.minecraft.entity.Entity;
import org.armedbear.lisp.LispError;
import org.armedbear.lisp.LispObject;

import static org.armedbear.lisp.Lisp.error;

public class LispEntity extends LispBaseObject<Entity> implements Vectorizable, Positionable {

	public LispEntity(Entity entity) {
		super(entity);
	}


	@Override
	public LispVec3d getVec3d() {
		return LispVec3d.fromVec3d(javaInstance().getPos());
	}

	@Override
	public LispBlockPos getBlockPos() {
		return LispBlockPos.fromBlockPos(javaInstance().getBlockPos());
	}
}
