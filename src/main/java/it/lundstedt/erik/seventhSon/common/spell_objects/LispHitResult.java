package it.lundstedt.erik.seventhSon.common.spell_objects;

import it.lundstedt.erik.seventhSon.common.spell_interfaces.Positionable;
import it.lundstedt.erik.seventhSon.common.spell_interfaces.Vectorizable;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import org.armedbear.lisp.*;

import javax.swing.text.html.parser.Entity;

import static org.armedbear.lisp.Lisp.error;

public class LispHitResult extends LispBaseObject<HitResult> implements Vectorizable, Positionable {

	public LispHitResult(HitResult result) {
		super(result);
	}

	@Override
	public LispVec3d getVec3d() {
		return LispVec3d.fromVec3d(javaInstance().getPos());
	}

	public AbstractString getType() {
		return new SimpleString(javaInstance().getType().toString());
	}

	public LispObject getEntity() {
		return javaInstance().getType().equals(HitResult.Type.ENTITY) ? new LispEntity(((EntityHitResult)javaInstance()).getEntity()) : Lisp.NIL;
	}

	@Override
	public LispBlockPos getBlockPos() {
		switch (javaInstance().getType()) {
			case BLOCK:
				return LispBlockPos.fromBlockPos(((BlockHitResult)innerObject).getBlockPos());
			case ENTITY:
				return LispBlockPos.fromBlockPos(((EntityHitResult)innerObject).getEntity().getBlockPos());
			default:
				return LispBlockPos.fromBlockPos(new BlockPos(innerObject.getPos()));
		}
	}

	public LispObject getSide() {
		return javaInstance().getType().equals(HitResult.Type.BLOCK) ? new SimpleString(((BlockHitResult)javaInstance()).getSide().asString()) : Lisp.NIL;
	}

	public boolean isInsideBlock() {
		return javaInstance().getType().equals(HitResult.Type.BLOCK) && ((BlockHitResult) javaInstance()).isInsideBlock();
	}
}
