package it.lundstedt.erik.seventhSon.common.spell_objects;

import it.lundstedt.erik.seventhSon.common.spell_interfaces.Positionable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import org.armedbear.lisp.LispObject;
import org.armedbear.lisp.Symbol;

import static org.armedbear.lisp.Lisp.type_error;

public class LispVec3d extends LispBaseObject<Vec3d> implements Positionable {

	public LispVec3d(Vec3d vec3d) {
		super(vec3d);
	}

	@Override
	public LispObject add(int n) {
		javaInstance().add(n, n, n);
		return this;
	}

	@Override
	public LispObject add(LispObject obj) {
		if (obj.numberp()) {
			javaInstance().add(obj.doubleValue(),  obj.doubleValue(),  obj.doubleValue());
		}
		if (obj instanceof LispVec3d) {
			LispVec3d pos = (LispVec3d) obj;
			javaInstance().add(pos.javaInstance());
			return this;
		}
		return type_error(this, Symbol.NUMBER);
	}

	@Override
	public LispObject subtract(int n) {
		return add(-n);
	}

	@Override
	public LispObject subtract(LispObject obj) {
		if (obj.numberp()) {
			javaInstance().subtract(obj.doubleValue(), obj.doubleValue(), obj.doubleValue());
			return this;
		}
		if (obj instanceof LispVec3d) {
			LispVec3d pos = (LispVec3d) obj;
			javaInstance().subtract(pos.javaInstance());
			return this;
		}
		return type_error(this, Symbol.NUMBER);
	}

	@Override
	public LispObject multiplyBy(int n) {
		javaInstance().multiply(n);
		return this;
	}

	@Override
	public LispObject multiplyBy(LispObject obj) {
		if (obj.numberp()) {
			javaInstance().multiply(obj.doubleValue());
			return this;
		}
		return type_error(this, Symbol.NUMBER);
	}

	@Override
	public LispObject divideBy(LispObject obj) {
		if (obj.numberp()) {
			javaInstance().multiply(1/obj.doubleValue());
			return this;
		}
		return type_error(this, Symbol.NUMBER);
	}

	public static LispVec3d fromVec3d(Vec3d vector) {
		return new LispVec3d(vector);
	}


	@Override
	public LispBlockPos getBlockPos() {
		return LispBlockPos.fromBlockPos(new BlockPos(innerObject));
	}
}
