package it.lundstedt.erik.seventhSon.compat;

import dan200.computercraft.shared.media.items.ItemPrintout;
import it.lundstedt.erik.seventhSon.api.TextRegistry;

public class ComputerCraftCompat {

	public static void init() {
		TextRegistry.getInstance().registerItem(item -> item.getItem() instanceof ItemPrintout, ItemPrintout::getText);
	}

}
