package it.lundstedt.erik.seventhSon.component;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import dev.onyxstudios.cca.api.v3.item.ItemComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.item.ItemComponentInitializer;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.items.SeventhSonItems;
import net.minecraft.entity.projectile.ProjectileEntity;

public class ComponentInitializer implements ItemComponentInitializer, EntityComponentInitializer {

	public static final ComponentKey<SpellCasterComponent> SPELL_CASTER = ComponentRegistryV3.INSTANCE.getOrCreate(SeventhSonMain.getId("spell_caster"), SpellCasterComponent.class);
	public static final ComponentKey<ProjectileComponent> PROJECTILE_COMPONENT = ComponentRegistryV3.INSTANCE.getOrCreate(SeventhSonMain.getId("projectile"), ProjectileComponent.class);
	public static final ComponentKey<PlayerManaComponent> PLAYER_MANA_COMPONENT = ComponentRegistryV3.INSTANCE.getOrCreate(SeventhSonMain.getId("player_mana"), PlayerManaComponent.class);

	@Override
	public void registerItemComponentFactories(ItemComponentFactoryRegistry registry) {
		registry.register(SeventhSonItems.spellCasterBasic, SPELL_CASTER, stack -> new SpellCasterComponent(stack, 9, 2));
	}

	@Override
	public void registerEntityComponentFactories(EntityComponentFactoryRegistry registry) {
		registry.registerFor(ProjectileEntity.class, PROJECTILE_COMPONENT, ProjectileComponent::new);
		registry.registerForPlayers(PLAYER_MANA_COMPONENT, PlayerManaComponent::new);
	}
}
