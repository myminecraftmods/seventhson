package it.lundstedt.erik.seventhSon.component;

import dev.onyxstudios.cca.api.v3.component.Component;
import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent;
import dev.onyxstudios.cca.api.v3.component.tick.CommonTickingComponent;
import dev.onyxstudios.cca.api.v3.entity.PlayerComponent;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;

import java.util.NoSuchElementException;
import java.util.Optional;

public class PlayerManaComponent implements PlayerComponent<Component>, CommonTickingComponent, AutoSyncedComponent {

	public static final String MANA = "Mana";
	public static final String MAX_MANA = "MaxMana";
	public static final String MANA_REGEN_RATE = "ManaRegenRate";
	public static final String LEVEL = "Level";
	public static final String EXP_PROGRESS = "ExpProgress";


	public final PlayerEntity player;

	private double prevMana = 0;
	private double mana = 0;

	public double maxMana = 500;
	public double manaRegenRate = 5;

	protected int level = 0;
	public double expProgress = 0;

	public PlayerManaComponent(PlayerEntity player) {
		this.player = player;
	}

	public static Optional<PlayerManaComponent> getComponent(PlayerEntity player) {
		if (player != null) {
			try {
				return Optional.of(ComponentInitializer.PLAYER_MANA_COMPONENT.get(player));
			} catch (NoSuchElementException ignored) {}
		}
		return Optional.empty();
	}

	@Override
	public void readFromNbt(CompoundTag tag) {
		mana = tag.getDouble(MANA);
		maxMana = tag.getDouble(MAX_MANA);
		manaRegenRate = tag.getDouble(MANA_REGEN_RATE);
		level = tag.getInt(LEVEL);
		expProgress = tag.getDouble(EXP_PROGRESS);
	}

	@Override
	public void writeToNbt(CompoundTag tag) {
		tag.putDouble(MANA, mana);
		tag.putDouble(MAX_MANA, maxMana);
		tag.putDouble(MANA_REGEN_RATE, manaRegenRate);
		tag.putInt(LEVEL, level);
		tag.putDouble(EXP_PROGRESS, expProgress);
	}

	@Override
	public boolean shouldSyncWith(ServerPlayerEntity player) {
		return player.equals(player);
	}

	@Override
	public void tick() {
		prevMana = mana;
		gainMana(manaRegenRate);
	}

	public boolean setMana(double mana) {
		if (mana >= 0 && mana <= maxMana) {
			this.mana = mana;
			return true;
		}
		return false;
	}

	public boolean gainMana(double amount) {
		if (mana + amount <= maxMana) {
			mana += amount;
			return true;
		}
		return false;
	}

	public boolean drainManaIfPossible(double amount, boolean simulate) {
		if (mana - amount >= 0) {
			if (!simulate) {
				mana -= amount;
			}
			return true;
		}
		return false;
	}

	public double drainMana(double amount, boolean simulate) {
		double after = Math.max(mana - amount, 0);
		double drain = mana - after;
		if (!simulate) {
			mana = after;
		}
		return drain;
	}

	public double getMana() {
		return mana;
	}

	public double getPrevMana() {
		return prevMana;
	}

	public boolean displayMana() {
		return maxMana > 0;
	}

	public void sync() {
		ComponentInitializer.PLAYER_MANA_COMPONENT.sync(player);
	}

	public int getLevel() {
		return level;
	}

	public void gainExperience(int amount) {
		expProgress += convertExperience(amount);
		while (expProgress >= 1) {
			expProgress--;
			levelUp();
		}
		while (expProgress <= -1) {
			expProgress++;
			levelDown();
		}
	}

	public double convertExperience(int amount) {
		return (double) amount / level != 0 ? level * 10 : 1;
	}

	public void levelUp() {
		setLevel(level+1);
	}

	public void levelDown() {
		setLevel(level-1);
	}

	public int setLevel(int newLevel) {
		if (newLevel < 0 || newLevel > getMaxLevel()) return level;
		onLevelChange(level, newLevel);
		return this.level = newLevel;
	}

	protected void onLevelChange(int oldLevel, int newLevel) {
		maxMana += getMaxManaIncrease(oldLevel, newLevel);
		manaRegenRate += getManaRegenRateIncrease(oldLevel, newLevel);
	}

	public int getMaxManaIncrease(int oldLevel, int newLevel) {
		return 100 * newLevel * (newLevel - oldLevel);
	}

	public int getManaRegenRateIncrease(int oldLevel, int newLevel) {
		return 5 * newLevel * (newLevel - oldLevel);
	}

	public int getMaxLevel() {
		return 2048;
	}
}
