package it.lundstedt.erik.seventhSon.component;


import dev.onyxstudios.cca.api.v3.component.tick.CommonTickingComponent;
import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.spell_components.action.Action;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.SaveableAction;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.ProjectileAttachable;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ProjectileComponent implements CommonTickingComponent {

	public static final String ACTIONS = "Actions";

	public final ProjectileEntity projectile;

	public List<ProjectileAttachable<?>> actions = new ArrayList<>();

	public ProjectileComponent(ProjectileEntity projectile) {
		this.projectile = projectile;
	}

	public static Optional<ProjectileComponent> getComponent(ProjectileEntity projectile) {
		try {
			return Optional.of(ComponentInitializer.PROJECTILE_COMPONENT.get(projectile));
		} catch (NoSuchElementException ignored) {
			return Optional.empty();
		}
	}

	public void addAction(ProjectileAttachable<?> action) {
		this.actions.add(action);
	}

	public boolean onCollision(HitResult hitResult) {
		for (ProjectileAttachable<?> action : actions) {
			if (!action.onCollision(hitResult)) {
				return false;
			}
		}
		return true;
	}

	public boolean onEntityHit(EntityHitResult entityHitResult) {
		for (ProjectileAttachable<?> action : actions) {
			if (!action.onEntityHit(entityHitResult)) {
				return false;
			}
		}
		return true;
	}

	public boolean onBlockHit(BlockHitResult blockHitResult) {
		for (ProjectileAttachable<?> action : actions) {
			if (!action.onBlockHit(blockHitResult)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void tick() {
		for (ProjectileAttachable<?> action : actions) {
			action.tick();
		}
	}

	@Override
	public void readFromNbt(CompoundTag tag) {
		CompoundTag actions = tag.getCompound(ACTIONS);
		for (String key : actions.getKeys()) {
			ActionRegistry.registry.getOrEmpty(new Identifier(key)).ifPresent(type -> {
				Action action = type.createType(null);
				if (action instanceof SaveableAction) {
					((SaveableAction)action).readFromNbt(tag.getCompound(key));
				}
				if (action instanceof ProjectileAttachable<?>) {
					((ProjectileAttachable<?>) action).setProjectile(projectile);
					this.actions.add((ProjectileAttachable<?>)action);
				}
			});
		}
	}

	@Override
	public void writeToNbt(CompoundTag tag) {
		CompoundTag actions = new CompoundTag();
		for (ProjectileAttachable<?> action : this.actions) {
			if (action instanceof Action) {
				Identifier id = ActionRegistry.registry.getId(((Action)action).getType());
				if (id != null) {
					actions.put(id.toString(), action.writeToNbt(new CompoundTag()));
				}
			}
		}
		tag.put(ACTIONS, actions);
	}
}
