package it.lundstedt.erik.seventhSon.component;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.item.ItemComponent;
import it.lundstedt.erik.seventhSon.common.items.SeventhSonItems;
import it.lundstedt.erik.seventhSon.common.registries.ActionRegistry;
import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import it.lundstedt.erik.seventhSon.common.screen_handler.SpellCasterBasicHandler;
import it.lundstedt.erik.seventhSon.common.spell_components.action.Action;
import it.lundstedt.erik.seventhSon.common.spell_components.type.Type;
import it.lundstedt.erik.seventhSon.util.helper.RegistryHelper;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class SpellCasterComponent extends ItemComponent implements Inventory, ExtendedScreenHandlerFactory {

	public final DefaultedList<ItemStack> inv;
	public final int actionSlotCount;
	public Type type = null;

	public SpellCasterComponent(ItemStack stack, int slots, int actionSlotCount) {
		this(stack, slots, actionSlotCount, ComponentInitializer.SPELL_CASTER);
	}

	public SpellCasterComponent(ItemStack stack, int slots, int actionSlotCount, ComponentKey<?> key) {
		super(stack, key);
		inv = DefaultedList.ofSize(slots, ItemStack.EMPTY);
		this.actionSlotCount = actionSlotCount;
		load();
	}

	public static Optional<SpellCasterComponent> getComponent(ItemStack stack) {
		if (stack.getItem().equals(SeventhSonItems.spellCasterBasic)) {
			try {
				return Optional.of(ComponentInitializer.SPELL_CASTER.get(stack));
			} catch (NoSuchElementException ignored) {}
		}
		return Optional.empty();
	}

	@Override
	public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
		buf.writeInt(getSlotForItemFromPlayer(player));
	}

	public int getSlotForItemFromPlayer(ServerPlayerEntity player) {
		return player.inventory.selectedSlot;
	}

	@Override
	public int size() {
		return inv.size();
	}

	@Override
	public int getMaxCountPerStack() {
		return 16;
	}

	@Override
	public boolean isEmpty() {
		for (ItemStack stack : inv) {
			if (!stack.isEmpty()) return false;
		}
		return true;
	}

	@Override
	public ItemStack getStack(int slot) {
		return inv.get(slot);
	}

	@Override
	public ItemStack removeStack(int slot, int amount) {
		ItemStack returnStack = inv.get(slot).split(amount);
		markDirty();
		return returnStack;
	}

	@Override
	public ItemStack removeStack(int slot) {
		ItemStack stack = inv.get(slot);
		inv.set(slot, ItemStack.EMPTY);
		markDirty();
		return stack;
	}

	@Override
	public void setStack(int slot, ItemStack stack) {
		inv.set(slot, stack.copy());
		markDirty();
	}

	@Override
	public void markDirty() {
		save();
	}

	@Override
	public boolean canPlayerUse(PlayerEntity player) {
		return true;
	}

	@Override
	public Text getDisplayName() {
		return stack.getName();
	}

	@Override
	public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
		return new SpellCasterBasicHandler(syncId, player.inventory, this);
	}

	@Override
	public void clear() {
		inv.clear();
		markDirty();
	}

	public void save() {
		Inventories.toTag(getOrCreateRootTag(), this.inv);
	}

	public void load() {
		CompoundTag rootTag = getOrCreateRootTag();
		if (rootTag.contains("Items")) {
			Inventories.fromTag(rootTag, this.inv);
		}
	}

	public Type getType() {
		ItemStack stack = getStack(0);
		if (type != null && type.createdFrom().test(stack)) {
			return type;
		} else {
			for (TypeRegistry.TypeType<?> typeType : RegistryHelper.getValues(TypeRegistry.registry)) {
				if (typeType.equals(TypeRegistry.dummy)) {
					continue;
				}
				Type type = typeType.createType(this);
				if (type.createdFrom().test(stack)) {
					this.type = type;
					return type;
				}
			}
		}
		return TypeRegistry.dummy.createType(this);
	}

	public Collection<Action> getActions(Type type) {
		Collection<Action> actions = new HashSet<>();
		for (ActionRegistry.ActionType<?> actionType : RegistryHelper.getValues(ActionRegistry.registry)) {
			Action action = actionType.createType(type);
			for (int i = 1; i <= actionSlotCount; i++) {
				ItemStack stack = getStack(i);
				if (action.createdFrom().test(stack)) {
					if (!actions.contains(action)) {
						actions.add(action);
					}
					action.magnitude++;
				}
			}
		}
		return actions;
	}

	public Collection<ItemStack> getModifiers() {
		Collection<ItemStack> actions = new ArrayList<>();
		for (int i = 3; i < size(); i++) {
			actions.add(getStack(i));
		}
		return actions;
	}

	public double getManaDrain() {
		Type type = getType();
		double drain = type.getManaDrain();
		for (Action action : type.getActions()) {
			drain += action.getManaDrain();
		}
		return drain;
	}

}
