package it.lundstedt.erik.seventhSon.mixin;

import org.armedbear.lisp.Lisp;
import org.armedbear.lisp.Package;
import org.armedbear.lisp.Packages;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(value = Lisp.class, remap = false)
public interface AccessorLisp {

	@Accessor("PACKAGE_CL")
	@Mutable
	static void setPackageCl(Package packageReplacement) {}

	@Accessor("PACKAGE_CL_USER")
	@Mutable
	static void setPackageClUser(Package packageReplacement) {}

	@Accessor("PACKAGE_KEYWORD")
	@Mutable
	static void setPackageKeyword(Package packageReplacement) {}

	@Accessor("PACKAGE_SYS")
	@Mutable
	static void setPackageSys(Package packageReplacement) {}

	@Accessor("PACKAGE_MOP")
	@Mutable
	static void setPackageMop(Package packageReplacement) {}

	@Accessor("PACKAGE_TPL")
	@Mutable
	static void setPackageTpl(Package packageReplacement) {}

	@Accessor("PACKAGE_EXT")
	@Mutable
	static void setPackageExt(Package packageReplacement) {}

	@Accessor("PACKAGE_JVM")
	@Mutable
	static void setPackageJvm(Package packageReplacement) {}

	@Accessor("PACKAGE_LOOP")
	@Mutable
	static void setPackageLoop(Package packageReplacement) {}

	@Accessor("PACKAGE_PROF")
	@Mutable
	static void setPackageProf(Package packageReplacement) {}

	@Accessor("PACKAGE_JAVA")
	@Mutable
	static void setPackageJava(Package packageReplacement) {}

	@Accessor("PACKAGE_LISP")
	@Mutable
	static void setPackageLisp(Package packageReplacement) {}

	@Accessor("PACKAGE_THREADS")
	@Mutable
	static void setPackageThreads(Package packageReplacement) {}

	@Accessor("PACKAGE_FORMAT")
	@Mutable
	static void setPackageFormat(Package packageReplacement) {}

	@Accessor("PACKAGE_XP")
	@Mutable
	static void setPackageXp(Package packageReplacement) {}

	@Accessor("PACKAGE_PRECOMPILER")
	@Mutable
	static void setPackagePrecompiler(Package packageReplacement) {}

	@Accessor("PACKAGE_SEQUENCE")
	@Mutable
	static void setPackageSequence(Package packageReplacement) {}





}
