package it.lundstedt.erik.seventhSon.mixin;

import org.armedbear.lisp.Package;
import org.armedbear.lisp.Symbol;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.concurrent.ConcurrentHashMap;

@Mixin(value = Package.class, remap = false)
public interface AccessorPackage {

	@Accessor
	ConcurrentHashMap<String, Symbol> getInternalSymbols();

	@Accessor
	ConcurrentHashMap<String, Symbol> getExternalSymbols();

}
