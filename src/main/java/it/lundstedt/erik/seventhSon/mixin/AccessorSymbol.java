package it.lundstedt.erik.seventhSon.mixin;

import org.armedbear.lisp.LispObject;
import org.armedbear.lisp.Symbol;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(value = Symbol.class, remap = false)
public interface AccessorSymbol {

	@Accessor
	@Mutable
	void setPkg(LispObject pkg);

}
