package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.DragChanger;
import it.lundstedt.erik.seventhSon.common.spell_components.action.interfaces.ProjectileAttachable;
import it.lundstedt.erik.seventhSon.component.ProjectileComponent;
import net.minecraft.entity.projectile.ExplosiveProjectileEntity;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.throwables.MixinException;

@Mixin(ExplosiveProjectileEntity.class)
public class MixinExplosiveProjectileEntity {

	@Shadow
	protected float getDrag() {
		throw new MixinException("Shadow failed");
	}

	@Redirect(method = "tick", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/ExplosiveProjectileEntity;getDrag()F", ordinal = 0))
	public float seventhSonModifyDrag(ExplosiveProjectileEntity explosiveProjectileEntity) {
		MutableFloat returnValue = new MutableFloat(getDrag());
		ProjectileComponent.getComponent(explosiveProjectileEntity).ifPresent(component -> {
			for (ProjectileAttachable<?> action : component.actions) {
				if (action instanceof DragChanger) {
					returnValue.setValue(((DragChanger)action).getDrag(getDrag(), returnValue.getValue()));
				}
			}
		});
		return returnValue.getValue();
	}

}
