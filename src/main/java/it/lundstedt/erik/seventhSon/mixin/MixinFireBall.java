package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import net.minecraft.entity.*;
import net.minecraft.entity.projectile.AbstractFireballEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(FireballEntity.class)
public class MixinFireBall extends AbstractFireballEntity {
	
	public MixinFireBall(EntityType<? extends AbstractFireballEntity> entityType, World world) {
		super(entityType, world);
	}
	
	@Inject(at = @At("HEAD"), method = "onCollision")
	protected void seventhSonOnFirebalLCollision(HitResult hitResult, CallbackInfo info) {
		if (!world.isClient() && hitResult.getType() != HitResult.Type.MISS && SeventhSonMain.config.fireballsAlwaysLightning) {
			//Vec3d pos = user.getPos().add(0, user.getEyeHeight(user.getPose()), 0);
			//Vec3d facing = user.getRotationVector();

			Vec3d pos = hitResult.getPos();
			//Vec3d pos2 = result.getPos();
			LightningEntity lightning = EntityType.LIGHTNING_BOLT.create(world);
			lightning.updatePosition(pos.getX(), pos.getY(), pos.getZ());
			Entity owner = getOwner();
			lightning.setChanneler(owner instanceof ServerPlayerEntity ? (ServerPlayerEntity)owner : null);
			lightning.setCosmetic(false);
			world.spawnEntity(lightning);
			//world.setBlockState(pos1, Blocks.BEDROCK.getDefaultState());

		}
	}
}
