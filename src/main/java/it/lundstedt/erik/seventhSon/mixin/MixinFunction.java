package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.util.LispFunctionCapability;
import org.armedbear.lisp.Function;
import org.armedbear.lisp.Package;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Function.class)
public class MixinFunction implements LispFunctionCapability {

	private FunctionSymbolState state = FunctionSymbolState.EXISTS;


	@Inject(method = "<init>(Ljava/lang/String;Ljava/lang/String;)V", at = @At("RETURN"))
	public void seventhSonInitFunction(String name, String arglist, CallbackInfo ci) {
		state = FunctionSymbolState.EXPORTS;
	}

	@Inject(method = "<init>(Ljava/lang/String;Lorg/armedbear/lisp/Package;ZLjava/lang/String;Ljava/lang/String;)V", at = @At("RETURN"))
	public void seventhSonInitFunctionWithPackage(String name, Package pkg, boolean exported, String arglist, String docstring, CallbackInfo ci) {
		this.state = FunctionSymbolState.makeCreationState(exported);
	}


	@Override
	public FunctionSymbolState getSymbolState() {
		return state;
	}
}
