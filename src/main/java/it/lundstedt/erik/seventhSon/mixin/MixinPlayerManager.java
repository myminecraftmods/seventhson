package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.network.ServerNetworkHandler;
import it.lundstedt.erik.seventhSon.util.ConfigCommons;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerManager.class)
public class MixinPlayerManager {

	@Shadow
	@Final
	private MinecraftServer server;

	@Inject(method = "onPlayerConnect", at = @At("RETURN"))
	public void seventhSonOnPlayerConnect(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci) {
		if (!server.isHost(player.getGameProfile())) {
			ServerNetworkHandler.sendTo(player, ServerNetworkHandler.syncConfig());
		} else {
			ConfigCommons.sync(SeventhSonMain.config);
		}
	}

}
