package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.component.ProjectileComponent;
import net.minecraft.block.BlockState;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ProjectileEntity.class)
public class MixinProjectileEntity {

	@Inject(method = "onCollision", at = @At("HEAD"), cancellable = true)
	protected void seventhSonOnCollision(HitResult hitResult, CallbackInfo ci) {
		ProjectileComponent.getComponent((ProjectileEntity)(Object)this).ifPresent(component -> {
			if (!component.onCollision(hitResult)) {
				ci.cancel();
			}
		});
	}

	@Inject(method = "onEntityHit", at = @At("HEAD"), cancellable = true)
	protected void seventhSonOnEntityHit(EntityHitResult entityHitResult, CallbackInfo ci) {
		ProjectileComponent.getComponent((ProjectileEntity)(Object)this).ifPresent(component -> {
			if (!component.onEntityHit(entityHitResult)) {
				ci.cancel();
			}
		});
	}

	@Inject(method = "onBlockHit", at = @At("HEAD"), cancellable = true)
	protected void seventhSonOnBlockHit(BlockHitResult blockHitResult, CallbackInfo ci) {
		ProjectileComponent.getComponent((ProjectileEntity)(Object)this).ifPresent(component -> {
			if (!component.onBlockHit(blockHitResult)) {
				ci.cancel();
			}
		});
	}

}
