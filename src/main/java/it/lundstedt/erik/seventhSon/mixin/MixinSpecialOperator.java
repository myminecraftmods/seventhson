package it.lundstedt.erik.seventhSon.mixin;

import it.lundstedt.erik.seventhSon.util.LispFunctionCapability;
import org.armedbear.lisp.Package;
import org.armedbear.lisp.SpecialOperator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(SpecialOperator.class)
public class MixinSpecialOperator implements LispFunctionCapability {

	private FunctionSymbolState state = FunctionSymbolState.EXISTS;

	@Inject(method = "<init>(Ljava/lang/String;Lorg/armedbear/lisp/Package;ZLjava/lang/String;)V", at = @At("RETURN"))
	public void seventhSonSpecialOperatorInit(String name, Package pkg, boolean exported, String arglist, CallbackInfo ci) {
		state = FunctionSymbolState.makeCreationState(exported);
	}

	@Override
	public FunctionSymbolState getSymbolState() {
		return state;
	}
}
