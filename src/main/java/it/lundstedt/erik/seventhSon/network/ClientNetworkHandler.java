package it.lundstedt.erik.seventhSon.network;

import io.netty.buffer.Unpooled;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.common.block_entities.LispMachineBlockEntity;
import it.lundstedt.erik.seventhSon.util.ConfigCommons;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.util.math.BlockPos;

import java.util.ArrayList;
import java.util.List;

public class ClientNetworkHandler {

	public static void init() {
		ClientPlayNetworking.registerGlobalReceiver(ServerNetworkHandler.S2C_SYNC_CONFIG, (client, handler, buf, responseSender) -> {
			int size = buf.readInt();
			List<String> headers = new ArrayList<>();
			for (int i = 0; i < size; i++) {
				headers.add(buf.readString());
			}
			client.execute(() -> {
				ConfigCommons.getInstance().lispHeaders = headers;
				SeventhSonLogger.info("The lisp headers on this server are: " + headers);
			});
		});
	}

	public static CustomPayloadC2SPacket activateSpell(BlockPos pos) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeBlockPos(pos);
		return new CustomPayloadC2SPacket(ServerNetworkHandler.C2S_USE_SPELL_BLOCK, buf);
	}

	public static CustomPayloadC2SPacket activateSpell(Entity entity) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeInt(entity.getEntityId());
		return new CustomPayloadC2SPacket(ServerNetworkHandler.C2S_USE_SPELL_ENTITY, buf);
	}

	public static CustomPayloadC2SPacket activateSpell(int inventorySlot) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeInt(inventorySlot);
		return new CustomPayloadC2SPacket(ServerNetworkHandler.C2S_USE_SPELL_ITEM, buf);
	}

	public static CustomPayloadC2SPacket sendTextStoringBlockData(LispMachineBlockEntity block) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeBlockPos(block.getPos());
		buf.writeInt(block.text.size());
		for (String line : block.text) {
			buf.writeString(line);
		}
		return new CustomPayloadC2SPacket(ServerNetworkHandler.C2S_SEND_TEXT_STORING_BLOCK_DATA, buf);
	}

	public static void sendPacket(CustomPayloadC2SPacket packet) {
		ClientPlayNetworking.getSender().sendPacket(packet);
	}

}
