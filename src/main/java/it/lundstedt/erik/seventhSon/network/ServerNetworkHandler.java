package it.lundstedt.erik.seventhSon.network;

import io.netty.buffer.Unpooled;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.common.block_entities.LispMachineBlockEntity;
import it.lundstedt.erik.seventhSon.util.helper.LispHelper;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ServerNetworkHandler {

	public static Identifier S2C_SYNC_CONFIG = SeventhSonMain.getId("s2c_sunc_config");

	public static Identifier C2S_USE_SPELL_BLOCK = SeventhSonMain.getId("c2s_use_spell_block");
	public static Identifier C2S_USE_SPELL_ENTITY = SeventhSonMain.getId("c2s_use_spell_entity");
	public static Identifier C2S_USE_SPELL_ITEM = SeventhSonMain.getId("c2s_use_spell_item");
	public static Identifier C2S_SEND_TEXT_STORING_BLOCK_DATA = SeventhSonMain.getId("c2s_send_text_storing_block_data");

	public static CustomPayloadS2CPacket syncConfig() {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeInt(SeventhSonMain.config.lispHeaders.size());
		for (String string : SeventhSonMain.config.lispHeaders) {
			buf.writeString(string);
		}
		return new CustomPayloadS2CPacket(S2C_SYNC_CONFIG, buf);
	}

	public static void sendTo(ServerPlayerEntity player, CustomPayloadS2CPacket packet) {
		ServerPlayNetworking.getSender(player).sendPacket(packet);
	}

	public static void sendToAll(MinecraftServer server, CustomPayloadS2CPacket packet, Predicate<ServerPlayerEntity> shouldSend) {
		for (ServerPlayerEntity player : server.getPlayerManager().getPlayerList()) {
			if (shouldSend.test(player)) {
				sendTo(player, packet);
			}
		}
	}

	public static void sendToAll(ServerWorld world, CustomPayloadS2CPacket packet, Predicate<ServerPlayerEntity> shouldSend) {
		for (ServerPlayerEntity player : world.getPlayers()) {
			if (shouldSend.test(player)) {
				sendTo(player, packet);
			}
		}
	}

	public static void sendToAllExceptHost(MinecraftServer server, CustomPayloadS2CPacket packet) {
		sendToAll(server, packet, player -> !server.isHost(player.getGameProfile()));
	}

	public static void init() {
		ServerPlayNetworking.registerGlobalReceiver(C2S_USE_SPELL_BLOCK, (server, player, handler, buf, responseSender) -> {
			BlockPos pos = buf.readBlockPos();
			server.execute(() -> {
				double maxDistance = 6;
				Vec3d playerEyePos = player.getPos().add(0, player.getEyeHeight(player.getPose()), 0);
				Vec3d furthestHitPos = playerEyePos.add(player.getRotationVector().multiply(maxDistance));
				BlockHitResult result = player.world.raycast(new RaycastContext(playerEyePos, furthestHitPos, RaycastContext.ShapeType.OUTLINE, RaycastContext.FluidHandling.NONE, player));
				if (result.getType().equals(HitResult.Type.BLOCK) && result.getBlockPos().equals(pos) && result.getPos().distanceTo(playerEyePos) <= maxDistance) {
					LispHelper.parseLisp(player, player.world, pos);
				}
			});
		});
		ServerPlayNetworking.registerGlobalReceiver(C2S_USE_SPELL_ENTITY, (server, player, handler, buf, responseSender) -> {
			int entityId = buf.readInt();
			server.execute(() -> {
				Entity entity = player.world.getEntityById(entityId);
				if (entity != null) {
					double maxDistance = 6;
					Vec3d playerEyePos = player.getPos().add(0, player.getEyeHeight(player.getPose()), 0);
					Vec3d facing = player.getRotationVector().multiply(maxDistance);
					EntityHitResult result = ProjectileUtil.getEntityCollision(player.world, player, playerEyePos, playerEyePos.add(facing), player.getBoundingBox().stretch(facing).expand(1.0D), hitEntity -> true);
					if (result != null && result.getEntity().equals(entity) && result.getPos().distanceTo(playerEyePos) <= maxDistance) {
						LispHelper.parseLisp(player, entity);
					}
				}
			});
		});
		ServerPlayNetworking.registerGlobalReceiver(C2S_USE_SPELL_ITEM, (server, player, handler, buf, responseSender) -> {
			int inventorySlot = buf.readInt();
			server.execute(() -> {
				LispHelper.parseLisp(player, player.inventory.getStack(inventorySlot));
			});
		});
		ServerPlayNetworking.registerGlobalReceiver(C2S_SEND_TEXT_STORING_BLOCK_DATA, (server, player, handler, buf, responseSender) -> {
			BlockPos pos = buf.readBlockPos();
			List<String> text = new ArrayList<>();
			int size = buf.readInt();
			for (int i = 0; i < size; i++) {
				text.add(buf.readString());
			}
			server.execute(() -> {
				if (player.getBlockPos().getSquaredDistance(pos) <= Math.pow(6, 2)+1) {
					BlockEntity blockEntity = player.world.getBlockEntity(pos);
					if (blockEntity instanceof LispMachineBlockEntity) {
						((LispMachineBlockEntity) blockEntity).setText(text);
					}
				}
			});
		});
	}

}
