package it.lundstedt.erik.seventhSon.userFriendly;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.common.spell_objects.LispVec3d;
import it.lundstedt.erik.seventhSon.util.InitSpells;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.util.math.Vec3d;
import org.armedbear.lisp.*;

public class Functions
{

	/*
    public Function(String name, Package pkg, boolean exported,
                    String arglist)
    {
        this(name, pkg, exported, arglist, null);
    }
	*/



	public static void initSpells(InitSpells.RegistrationFunction registry,LispPackages packages) {

		/*Symbol sm=new Symbol("testSymbol");
		sm.setPackage(packages.helpful);


		registry.register(sm);

		registry.register("ghastBall", context -> new Function("ghastBall", packages.helpful,true, "") {

			@Override
			public LispObject execute() {
				return execute(new LispObject[]{});
			}

			@Override
			public LispObject execute(LispObject arg) {
				return execute(new LispObject[]{arg});
			}

			@Override
			public LispObject execute(LispObject[] params) {
				int pow = 10;
				if (params.length >= 1 && params[0] instanceof LispInteger) {
					LispInteger lInt= (LispInteger) params[0];
					pow =  lInt.intValue();
				}

				Vec3d pos = context.caster.getPos();
				if (params.length >= 2 && params[1] instanceof LispVec3d) {
					pos = ((LispVec3d)params[1]).javaInstance();
				}


				//LightningEntity entity = EntityType.LIGHTNING_BOLT.create(context.player.world);
				FireballEntity entity = EntityType.FIREBALL.create(context.caster.getWorld());
				entity.explosionPower=pow;
				SeventhSonLogger.info(pow);

				entity.refreshPositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
				context.caster.getWorld().spawnEntity(entity);



				return Lisp.NIL;
			}
		});*/


		registry.register("fireball", context -> new Function("fireball", "") {

			@Override
			public LispObject execute() {
				return execute(new LispObject[]{});
			}

			@Override
			public LispObject execute(LispObject arg) {
				return execute(new LispObject[]{arg});
			}

			@Override
			public LispObject execute(LispObject arg1, LispObject arg2) {
				return execute(new LispObject[]{arg1, arg2});
			}

			@Override
			public LispObject execute(LispObject[] params) {
				int pow = 10;
				if (params.length >= 1 && params[0] instanceof LispInteger) {
					LispInteger lInt= (LispInteger) params[0];
					pow =  lInt.intValue();
				}

				Vec3d pos = context.caster.getPos();
				if (params.length >= 2 && params[1] instanceof LispVec3d) {
					pos = ((LispVec3d)params[1]).javaInstance();
				}





				//LightningEntity entity = EntityType.LIGHTNING_BOLT.create(context.player.world);
				FireballEntity entity = EntityType.FIREBALL.create(context.caster.getWorld());
				//entity.setCosmetic(false);
				entity.explosionPower=pow;
				entity.setOnFireFor(pow);
				SeventhSonLogger.info(pow);

				entity.refreshPositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
				context.caster.getWorld().spawnEntity(entity);



				return Lisp.NIL;
			}
		});

	}
}
