package it.lundstedt.erik.seventhSon.util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.tag.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockReference {

	public final World world;
	public final BlockPos pos;

	public BlockReference(World world, BlockPos pos) {
		this.world = world;
		this.pos = pos;
	}

	public BlockState getBlockState() {
		return world.getBlockState(pos);
	}

	public BlockEntity getBlockEntity() {
		return world.getBlockEntity(pos);
	}

	public boolean isOf(Block block) {
		return getBlockState().isOf(block);
	}

	public boolean isIn(Tag<Block> tag) {
		return getBlockState().isIn(tag);
	}

}
