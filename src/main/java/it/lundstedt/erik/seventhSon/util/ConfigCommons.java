package it.lundstedt.erik.seventhSon.util;

import it.lundstedt.erik.seventhSon.SeventhSonConfig;

import java.util.ArrayList;
import java.util.List;

public class ConfigCommons {

	//This should remain volatile.
	private static volatile ConfigCommons instance = new ConfigCommons();

	public static synchronized ConfigCommons getInstance() {
		return instance;
	}

	public List<String> lispHeaders = new ArrayList<>();



	//The server is not available when we set this, ok? If you have a better idea for how to solve this then please share it.
	public static volatile boolean shouldSync = false;

	public static void sync(SeventhSonConfig config) {
		ConfigCommons.getInstance().lispHeaders = new ArrayList<>(config.lispHeaders);
	}
}
