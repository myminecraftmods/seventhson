package it.lundstedt.erik.seventhSon.util;

import com.mojang.datafixers.util.Either;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.Optional;
import java.util.Random;
import java.util.function.Function;

public class EntityOrBlockEntity {

	public final Either<Entity, BlockEntity> either;

	public EntityOrBlockEntity(Either<Entity, BlockEntity> either) {
		this.either = either;
	}

	public EntityOrBlockEntity(Entity entity) {
		this(Either.left(entity));
	}

	public EntityOrBlockEntity(BlockEntity blockEntity) {
		this(Either.right(blockEntity));
	}

	public Vec3d getPos() {
		return get(Entity::getPos, blockEntity -> Vec3d.ofBottomCenter(blockEntity.getPos()));
	}

	public BlockPos getBlockPos() {
		return get(Entity::getBlockPos, BlockEntity::getPos);
	}

	public World getWorld() { //We can assume that BlockEntity::getWorld will never be null as spells won't be executed when it's null.
		return get(Entity::getEntityWorld, BlockEntity::getWorld);
	}

	public MinecraftServer getServer() {
		return get(Entity::getServer, blockEntity -> blockEntity.getWorld().getServer());
	}

	public ServerWorld getServerWorld() {
		return (ServerWorld) getWorld();
	}

	public Random getRandom() {
		return get(
				entity -> entity instanceof LivingEntity ? ((LivingEntity)entity).getRandom() : entity.world.getRandom(),
				blockEntity -> blockEntity.getWorld().getRandom()
		);
	}

	/**
	 * This gets the entity if it's an entity.
	 * Remember that it's always nice if you implement for {@link EntityOrBlockEntity#getBlockEntity()} as well for compatibility.
	 * @return Returns an Optional containing an entity if {@link EntityOrBlockEntity#either} is an entity.
	 * Otherwise returns empty optional.
	 */
	public Optional<Entity> getEntity() {
		return either.left();
	}

	/**
	 * This gets the block entity if it's a block entity.
	 * Remember that it's always nice if you implement for {@link EntityOrBlockEntity#getEntity()} as well for compatibility.
	 * @return Returns an Optional containing a block entity if {@link EntityOrBlockEntity#either} is a block entity.
	 * Otherwise returns empty optional.
	 */
	public Optional<BlockEntity> getBlockEntity() {
		return either.right();
	}

	public <T> T get(Function<Entity, T> entityGetter, Function<BlockEntity, T> blockEntityGetter) {
		T object = either.right().map(blockEntityGetter).orElse(null);
		if (object == null) {
			object = either.left().map(entityGetter).orElse(null);
		}
		return object;
	}



	/**
	 * Only use this if the code only works on players.
	 * Otherwise use {@link EntityOrBlockEntity#getEntity()} or {@link EntityOrBlockEntity#getLivingEntity()}
	 * if you need living entities.
	 * @return Returns an Optional containing a player if {@link EntityOrBlockEntity#either} is a player.
	 * Otherwise returns empty optional.
	 */
	public Optional<ServerPlayerEntity> getPlayer() {
		Entity entity = getEntity().orElse(null);
		if (entity instanceof ServerPlayerEntity) {
			return Optional.of((ServerPlayerEntity) entity);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * If the code works on any entity, then use {@link EntityOrBlockEntity#getEntity()} instead.
	 * @return Returns an Optional containing a living entity if {@link EntityOrBlockEntity#either} is a living entity.
	 * Otherwise returns empty optional.
	 */
	public Optional<LivingEntity> getLivingEntity() {
		Entity entity = getEntity().orElse(null);
		if (entity instanceof LivingEntity) {
			return Optional.of((LivingEntity) entity);
		} else {
			return Optional.empty();
		}
	}

}
