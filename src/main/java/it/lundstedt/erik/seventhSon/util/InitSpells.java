package it.lundstedt.erik.seventhSon.util;

import net.minecraft.server.network.ServerPlayerEntity;
import org.armedbear.lisp.Symbol;

import java.util.function.Function;

@FunctionalInterface
public interface InitSpells {

	void initSpells(RegistrationFunction registry);

	interface RegistrationFunction {
		void register(String name, Function<PlayerScriptContext, Object> object);
		void register(Symbol symbol);
	}

}
