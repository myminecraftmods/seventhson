package it.lundstedt.erik.seventhSon.util;

import net.minecraft.item.ItemStack;
import net.minecraft.predicate.item.ItemPredicate;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Predicate;

public class ItemPredicates implements Predicate<ItemStack> {

	public static ItemPredicates NONE = new ItemPredicates();

	Collection<ItemPredicate> predicates;

	public ItemPredicates(ItemPredicate... predicates) {
		this(new HashSet<>(Arrays.asList(predicates)));
	}

	public ItemPredicates(Collection<ItemPredicate> predicates) {
		this.predicates = predicates;
	}

	@Override
	public boolean test(ItemStack stack) {
		for (ItemPredicate predicate : predicates) {
			if (predicate.test(stack)) {
				return true;
			}
		}
		return false;
	}

	public ItemPredicates add(ItemPredicates other) {
		predicates.addAll(other.predicates);
		return this;
	}

	public ItemPredicates add(ItemPredicate other) {
		predicates.add(other);
		return this;
	}

}
