package it.lundstedt.erik.seventhSon.util;

public interface LispFunctionCapability {

	FunctionSymbolState getSymbolState();


	public enum FunctionSymbolState {
		EXISTS,
		CREATES,
		EXPORTS;

		public boolean isExported() {
			return this == EXPORTS;
		}

		public boolean shouldAddSymbol() {
			return this != EXISTS;
		}

		public static FunctionSymbolState makeCreationState(boolean exported) {
			return exported ? FunctionSymbolState.EXPORTS : FunctionSymbolState.CREATES;
		}
	}

}
