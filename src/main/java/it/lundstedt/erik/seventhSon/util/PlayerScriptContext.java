package it.lundstedt.erik.seventhSon.util;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.api.GetBlockPosCallback;
import it.lundstedt.erik.seventhSon.api.GetPosCallback;
import it.lundstedt.erik.seventhSon.common.registries.SpellFunctionRegistry;
import it.lundstedt.erik.seventhSon.component.PlayerManaComponent;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Style;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import org.jetbrains.annotations.NotNull;

import javax.script.SimpleBindings;
import javax.script.SimpleScriptContext;
import java.io.*;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;


public class PlayerScriptContext extends SimpleScriptContext {

	public final EntityOrBlockEntity caster;
	public final double manaFactor;

	public final Object self;


	public boolean drainManaIfPossible(double amount, boolean simulate) {
		PlayerEntity player = caster.getPlayer().orElse(null);
		if (player != null) {
			PlayerManaComponent component = PlayerManaComponent.getComponent(player).orElse(null);
			if (component != null) {
				boolean success = component.drainManaIfPossible(manaFactor * amount, simulate);
				if (success && !simulate) {
					component.sync();
				}
				return success;
			} else {
				return false;
			}
		}
		return true; //TODO Mana system for mobs.
	}

	public double drainMana(double amount, boolean simulate) {
		PlayerEntity player = caster.getPlayer().orElse(null);
		if (player != null) {
			PlayerManaComponent component = PlayerManaComponent.getComponent(player).orElse(null);
			if (component != null) {
				double drain = component.drainMana(manaFactor * amount, simulate);
				if (drain > 0 && !simulate) {
					component.sync();
				}
				return drain;
			} else {
				return 0;
			}
		}
		return amount; //TODO Mana system for mobs.
	}

	public double modifyBasedOnMana(double toModify, double drain, boolean simulate) {
		if (drain == 0) return 0;
		return toModify * drainMana(drain, simulate) / drain;
	}

	public Optional<Entity> getSelfIfEntity() {
		if (self instanceof Entity) {
			return Optional.of((Entity) self);
		} else {
			return Optional.empty();
		}
	}

	public Optional<BlockReference> getSelfIfBlock() {
		if (self instanceof BlockReference) {
			return Optional.of((BlockReference) self);
		} else {
			return Optional.empty();
		}
	}

	public Optional<ItemStack> getSelfIfItemStack() {
		if (self instanceof ItemStack) {
			return Optional.of((ItemStack) self);
		} else {
			return Optional.empty();
		}
	}

	public Optional<Vec3d> getPosOfSelf() {
		return getDataFromSelf(Entity::getPos, reference -> Vec3d.ofBottomCenter(reference.pos),
				tile -> Vec3d.ofBottomCenter(tile.getPos()), GetPosCallback.EVENT.invoker()::getPos);
	}

	public Optional<BlockPos> getBlockPosOfSelf() {
		return getDataFromSelf(Entity::getBlockPos, reference -> reference.pos, BlockEntity::getPos,
				GetBlockPosCallback.EVENT.invoker()::getPos);
	}

	public <T> Optional<T> getDataFromSelf(Function<Entity, T> getPosFromEntity,
	                                       Function<BlockReference, T> getPosFromBlock,
	                                       Function<BlockEntity, T> getPosFromBlockEntity,
	                                       CallbackFunction<T> callbackFunction) {
		T pos = getSelfIfBlock().map(getPosFromBlock).orElse(null);
		if (pos == null) {
			pos = getSelfIfEntity().map(getPosFromEntity).orElse(null);
		}
		if (pos == null && getSelfIfItemStack().isPresent()) {
			pos = caster.get(getPosFromEntity, getPosFromBlockEntity);
		}
		return Optional.ofNullable(callbackFunction.getPos(self, caster, pos));
	}


	public PlayerScriptContext(EntityOrBlockEntity caster, double manaFactor, Object self) {
		this.caster = caster;
		this.manaFactor = manaFactor;
		this.self = self;
		engineScope = new SimpleBindings();





		engineScope.putAll(SpellFunctionRegistry.spells.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				entry -> entry.getValue().apply(this))));





		globalScope = null;
		reader = new InputStreamReader(System.in);
		ServerPlayerEntity player = caster.getPlayer().orElse(null);
		if (player != null) {
			writer = new ChatWriter(player, Style.EMPTY);
			errorWriter = new ChatWriter(player, Style.EMPTY.withColor(Formatting.RED));
		} else {
			writer = new SimpleWriter(SeventhSonLogger::info);
			errorWriter = new SimpleWriter(SeventhSonLogger::error);
		}
	}

	/*public static class ChatReader extends Reader {

		@Override
		public int read(@NotNull char[] cbuf, int off, int len) throws IOException {
			return 0;
		}

		@Override
		public void close() throws IOException {

		}
	}*/

	public static class ChatWriter extends SimpleWriter {

		public ChatWriter(PlayerEntity player, Style style) {
			super(message -> player.sendMessage(new LiteralText(message).setStyle(style), false));
		}
	}

	public static class SimpleWriter extends Writer {

		private StringBuilder message = null;

		public Consumer<String> printer;

		public SimpleWriter(Consumer<String> printer) {
			this.printer = printer;
		}

		@Override
		public void write(@NotNull char[] cbuf, int off, int len) throws IOException {
			if (message == null) {
				message = new StringBuilder();
			}
			for (int i = off; i < off + len; i++) {
				char toAppend = cbuf[i];
				switch (toAppend) {
					case '\r':
						continue;
					case '\n':
						flush();
						continue;
					default:
						message.append(toAppend);
				}
			}
		}

		@Override
		public void flush() throws IOException {
			if (message != null) {
				printer.accept(message.toString());
				message = null;
			}
		}

		@Override
		public void close() throws IOException {
			flush();
		}
	}

	@FunctionalInterface
	public interface CallbackFunction<T> {
		T getPos(Object self, EntityOrBlockEntity caster, T prevPos);
	}

}
