package it.lundstedt.erik.seventhSon.util.helper;

import it.lundstedt.erik.seventhSon.compat.ComputerCraftCompat;
import net.fabricmc.loader.api.FabricLoader;

public class CompatHelper {

	public static boolean isComputerCraftLoaded() {
		return FabricLoader.getInstance().isModLoaded("computercraft");
	}

	public static void init() {
		if (isComputerCraftLoaded()) {
			ComputerCraftCompat.init();
		}
	}

}
