package it.lundstedt.erik.seventhSon.util.helper;

import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.SeventhSonMain;
import it.lundstedt.erik.seventhSon.util.EntityOrBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.*;
import net.minecraft.util.Formatting;

public class ExceptionHelper {

	public static void printExceptionToChat(EntityOrBlockEntity caster, Throwable e) {
		PlayerEntity player = caster.getPlayer().orElse(null);
		if (player != null) {
			StringBuilder error = new StringBuilder();
			error.append(e).append("\n");
			for (StackTraceElement element : e.getStackTrace()) {
				error.append("\tat ").append(element).append("\n");
			}
			/*for (Throwable se : e.getSuppressed()) {
				se.printEnclosedStackTrace(s, trace, SUPPRESSED_CAPTION, "\t", dejaVu);
			}

			// Print cause, if any
			Throwable ourCause = e.getCause();
			if (ourCause != null) {
				ourCause.printEnclosedStackTrace(s, trace, CAUSE_CAPTION, "", dejaVu);
			}*/

			//HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(error.toString()));
			ClickEvent onClick = new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, error.toString());
			MutableText text = SeventhSonMain.getTranslatableText("script", "error", e.getLocalizedMessage());
			text = text.append(new LiteralText("  ")).append(SeventhSonMain.getTranslatableText("script", "clipboard")
					.setStyle(Style.EMPTY.withFormatting(Formatting.UNDERLINE).withClickEvent(onClick)));
			player.sendMessage(text, false);
		} else {
			SeventhSonLogger.error(e);
			e.printStackTrace();
		}
	}

}
