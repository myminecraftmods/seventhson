package it.lundstedt.erik.seventhSon.util.helper;

import com.mojang.datafixers.util.Either;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FileHelper {

	public static String writePathToString(Path path) {
		return path.toString().replaceAll("\\\\", "/");
	}

	public static List<File> getFilesInFolderAndSubFolders(File folder) {
		List<File> files = new ArrayList<>();
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				files.addAll(getFilesInFolderAndSubFolders(file));
			} else {
				files.add(file);
			}
		}
		return files;
	}

	public static List<File> listFilesRecursively(File folder) {
		return listFilesRecursively(folder, Optional.empty());
	}

	public static List<File> listFilesRecursively(File folder, Either<FileFilter, FilenameFilter> filter) {
		return listFilesRecursively(folder, Optional.of(filter));
	}

	public static List<File> listFilesRecursively(File folder, FileFilter filter) {
		return listFilesRecursively(folder, Either.left(filter));
	}

	public static List<File> listFilesRecursively(File folder, FilenameFilter filter) {
		return listFilesRecursively(folder, Either.right(filter));
	}

	public static List<File> listFilesRecursively(File folder, Optional<Either<FileFilter, FilenameFilter>> filter) {
		File[] fileArray = null;
		if (filter.isPresent()) {
			if (filter.get().left().isPresent()) {
				fileArray = folder.listFiles(filter.get().left().get());
			} else if (filter.get().right().isPresent()) {
				fileArray = folder.listFiles(filter.get().right().get());
			}
		} else {
			fileArray = folder.listFiles();
		}
		List<File> files = new ArrayList<>(Arrays.asList(Optional.ofNullable(fileArray).orElse(new File[0])));
		List<File> moreFiles = new ArrayList<>();
		for (File file : files) {
			moreFiles.addAll(listFilesRecursively(file, filter));
		}
		files.addAll(moreFiles);
		return files;
	}

}
