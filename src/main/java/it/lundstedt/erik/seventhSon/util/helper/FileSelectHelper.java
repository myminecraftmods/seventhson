package it.lundstedt.erik.seventhSon.util.helper;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.util.tinyfd.TinyFileDialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This file originated from Acuadragon100's currently private API Mod, it might be released to the public at some point in the future.
 * Don't worry, while it's in this project it goes under the same licence as the project.
 */
public class FileSelectHelper {

	public static <T> T prepareDialogUtils(DialogHandler<T> handler, File startingPath, String acceptedFile, String... otherAcceptedFiles) {
		//TinyFileDialogs is just weird, credits to    Source Project: LWJGUI   Source File: LWJGUIDialog.java    License: MIT License.

		MemoryStack stack = MemoryStack.stackPush();

		PointerBuffer acceptedFiles = null;
		if (acceptedFile != null) {
			acceptedFiles = stack.mallocPointer(1 + otherAcceptedFiles.length);

			acceptedFiles.put(stack.UTF8(acceptedFile));
			for (String name : otherAcceptedFiles) {
				if (name != null) {
					acceptedFiles.put(stack.UTF8(name));
				}
			}

			acceptedFiles.flip();
		}

		String startingPathString = null;
		if (startingPath != null) {
			startingPath = startingPath.getAbsoluteFile();
			startingPathString = startingPath.getAbsolutePath();
			if(startingPath.isDirectory() && !startingPathString.endsWith(File.separator)){
				startingPathString += File.separator;
			}
		}

		T value = handler.handleDialog(startingPathString, acceptedFiles);

		stack.pop();
		return value;
	}

	public static Optional<File> openSaveDialogFileSpecific(String title, File startingPath, String filterDescription, String acceptedFile, String... otherFiles) {
		String file = prepareDialogUtils(((pathString, acceptedFiles) -> {
			return TinyFileDialogs.tinyfd_saveFileDialog(title, pathString, acceptedFiles, filterDescription);
		}), startingPath, acceptedFile, otherFiles);

		return Optional.ofNullable(file).map(File::new);
	}

	public static Optional<File> openSaveDialog(String title, File startingPath, boolean enforceExtension, String filterDescription, String acceptedFileExtension, String... otherFileExtensions) {
		String file = prepareDialogUtils(((pathString, acceptedFiles) -> {
			return TinyFileDialogs.tinyfd_saveFileDialog(title, pathString, acceptedFiles, filterDescription);
		}), startingPath, "*." + acceptedFileExtension, Arrays.stream(otherFileExtensions).map(string -> "*." + string).toArray(String[]::new));

		if (file != null && enforceExtension) {
			boolean endsWithExtension = file.endsWith(acceptedFileExtension);
			if (!endsWithExtension) {
				for (String otherExtension : otherFileExtensions) {
					if (file.endsWith(otherExtension)) {
						endsWithExtension = true;
						break;
					}
				}
			}
			if (!endsWithExtension) {
				file += acceptedFileExtension;
			}
		}

		return Optional.ofNullable(file).map(File::new);
	}

	public static List<File> openLoadDialogFileSpecific(String title, File startingPath, boolean multiSelect, String filterDescription, String acceptedFile, String... otherFiles) {
		String file = prepareDialogUtils(((pathString, acceptedFiles) -> {
			return TinyFileDialogs.tinyfd_openFileDialog(title, pathString, acceptedFiles, filterDescription, multiSelect);
		}), startingPath, acceptedFile, otherFiles);

		return file != null ? Arrays.stream(file.split("\\|")).map(File::new).collect(Collectors.toList()) : new ArrayList<>();
	}

	public static List<File> openLoadDialog(String title, File startingPath, boolean multiSelect, String filterDescription, String acceptedFileExtension, String... otherFileExtensions) {
		return openLoadDialogFileSpecific(title, startingPath, multiSelect, filterDescription, "*." + acceptedFileExtension, Arrays.stream(otherFileExtensions).map(string -> "*." + string).toArray(String[]::new));
	}

	public static Optional<File> openLoadDialog(String title, File startingPath, String filterDescription, String acceptedFileExtension, String... otherFileExtensions) {
		return openLoadDialog(title, startingPath, false, filterDescription, acceptedFileExtension, otherFileExtensions).stream().findAny();
	}

	@FunctionalInterface
	public interface DialogHandler<T> {
		T handleDialog(String pathString, PointerBuffer acceptedFiles);
	}

}
