package it.lundstedt.erik.seventhSon.util.helper;

import com.google.common.collect.Lists;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.potion.Potion;
import net.minecraft.predicate.NbtPredicate;
import net.minecraft.predicate.NumberRange;
import net.minecraft.predicate.item.EnchantmentPredicate;
import net.minecraft.predicate.item.ItemPredicate;
import net.minecraft.tag.Tag;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public class ItemPredicateHelper {

	/*public static ItemPredicate fromItem(Item item) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTag(Tag<Item> tag) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromStackableItemStoredEnchants(Item item, NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableItemStoredEnchants(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromStackableItemAppliedEnchants(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromDamageableItemAppliedEnchants(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromItem(Item item, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromItem(Item item, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromItem(Item item, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromItemStoredEnchants(Item item, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromItemAppliedEnchants(Item item, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, Potion potion) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, Potion potion) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableItem(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableItem(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableItemStoredEnchants(Item item, NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableItemStoredEnchants(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableItemAppliedEnchants(Item item, NumberRange.IntRange count, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(null, item, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableItemAppliedEnchants(Item item, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromItem(Item item, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromItem(Item item, Potion potion) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromItem(Item item, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromItemStoredEnchants(Item item, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromItemAppliedEnchants(Item item, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(null, item, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}




	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromStackableItemStoredEnchants(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableItemStoredEnchants(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromStackableItemAppliedEnchants(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromDamageableItemAppliedEnchants(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromItemStoredEnchants(Tag<Item> tag, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromItemAppliedEnchants(Tag<Item> tag, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, Potion potion) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, Potion potion) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableTag(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableTag(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableTagStoredEnchants(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableTagStoredEnchants(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableTagAppliedEnchants(Tag<Item> tag, NumberRange.IntRange count, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(tag, null, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableTagAppliedEnchants(Tag<Item> tag, NumberRange.IntRange durability, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, Potion potion) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTag(Tag<Item> tag, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTagStoredEnchants(Tag<Item> tag, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromTagAppliedEnchants(Tag<Item> tag, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(tag, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}





	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromStackableItemStoredEnchants(NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromDamageableItemAppliedEnchants(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromStackableItemAppliedEnchants(NumberRange.IntRange count, EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromDamageableItemStoredEnchants(NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromAny(EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, nbt);
	}

	public static ItemPredicate fromAny(Potion potion, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, nbt);
	}

	public static ItemPredicate fromAny(EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromItemStoredEnchants(EnchantmentPredicate[] storedEnchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, nbt);
	}

	public static ItemPredicate fromAnyAppliedEnchants(EnchantmentPredicate[] enchantments, NbtPredicate nbt) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, nbt);
	}

	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, Potion potion) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, Potion potion) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableAny(NumberRange.IntRange count, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableAny(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableAnyStoredEnchants(NumberRange.IntRange count, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableAnyAppliedEnchantments(NumberRange.IntRange durability, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromStackableAnyAppliedEnchantments(NumberRange.IntRange count, EnchantmentPredicate[] enchantments) {
		return new ItemPredicate(null, null, count, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromDamageableAnyStoredEnchants(NumberRange.IntRange durability, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, durability, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAny(EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments, Potion potion) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAny(Potion potion) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], new EnchantmentPredicate[0], potion, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAny(EnchantmentPredicate[] enchantments, EnchantmentPredicate[] storedEnchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAnyStoredEnchants(EnchantmentPredicate... storedEnchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, new EnchantmentPredicate[0], storedEnchantments, null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAnyAppliedEnchants(EnchantmentPredicate... enchantments) {
		return new ItemPredicate(null, null, NumberRange.IntRange.ANY, NumberRange.IntRange.ANY, enchantments, new EnchantmentPredicate[0], null, NbtPredicate.ANY);
	}

	public static ItemPredicate fromAny() {
		return ItemPredicate.ANY;
	}
	
	public static boolean check(Collection<ItemPredicate> predicates, ItemStack item) {
		for (ItemPredicate predicate : predicates) {
			if (predicate.test(item)) {
				return true;
			}
		}
		return false;
	}*/

	public static Builder createBuilder() {
		return new Builder();
	}

	public static class Builder {
		protected final List<EnchantmentPredicate> enchantments = Lists.newArrayList();
		protected final List<EnchantmentPredicate> storedEnchantments = Lists.newArrayList();
		@Nullable
		protected Item item;
		@Nullable
		protected Tag<Item> tag;
		protected NumberRange.IntRange count;
		protected NumberRange.IntRange durability;
		@Nullable
		protected Potion potion;
		protected NbtPredicate nbt;

		public Builder() {
			this.count = NumberRange.IntRange.ANY;
			this.durability = NumberRange.IntRange.ANY;
			this.nbt = NbtPredicate.ANY;
		}

		public ItemPredicateHelper.Builder item(ItemConvertible item) {
			this.item = item.asItem();
			return this;
		}

		public ItemPredicateHelper.Builder tag(Tag<Item> tag) {
			this.tag = tag;
			return this;
		}

		public ItemPredicateHelper.Builder nbt(NbtPredicate nbt) {
			this.nbt = nbt;
			return this;
		}

		public ItemPredicateHelper.Builder enchantment(EnchantmentPredicate enchantment) {
			this.enchantments.add(enchantment);
			return this;
		}

		public ItemPredicateHelper.Builder storedEnchantment(EnchantmentPredicate enchantment) {
			this.storedEnchantments.add(enchantment);
			return this;
		}

		public ItemPredicateHelper.Builder potion(Potion potion) {
			this.potion = potion;
			return this;
		}

		public ItemPredicateHelper.Builder count(NumberRange.IntRange count) {
			this.count = count;
			return this;
		}

		public ItemPredicateHelper.Builder durability(NumberRange.IntRange durability) {
			this.durability = durability;
			return this;
		}

		public ItemPredicate build() {
			return new ItemPredicate(this.tag, this.item, this.count, this.durability, this.enchantments.toArray(EnchantmentPredicate.ARRAY_OF_ANY), this.storedEnchantments.toArray(EnchantmentPredicate.ARRAY_OF_ANY), this.potion, this.nbt);
		}
	}

}
