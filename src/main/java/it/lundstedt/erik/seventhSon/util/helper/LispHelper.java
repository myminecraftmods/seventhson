package it.lundstedt.erik.seventhSon.util.helper;

import com.google.common.collect.ImmutableList;
import it.lundstedt.erik.seventhSon.SeventhSonLogger;
import it.lundstedt.erik.seventhSon.api.*;
import it.lundstedt.erik.seventhSon.component.PlayerManaComponent;
import it.lundstedt.erik.seventhSon.mixin.AccessorLisp;
import it.lundstedt.erik.seventhSon.mixin.AccessorMinecraftServer;
import it.lundstedt.erik.seventhSon.mixin.AccessorPackage;
import it.lundstedt.erik.seventhSon.mixin.AccessorSymbol;
import it.lundstedt.erik.seventhSon.util.*;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.apache.commons.io.IOUtils;
import org.armedbear.lisp.*;
import org.armedbear.lisp.Package;
import org.armedbear.lisp.scripting.AbclScriptEngine;
import org.armedbear.lisp.scripting.AbclScriptEngineFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.armedbear.lisp.Lisp.PACKAGE_KEYWORD;


public class LispHelper {

	public static List<String> validFileEndings = new ArrayList<>(ImmutableList.of(
			".scm", ".ss", ".lisp", ".clj", ".cljs", ".cljc", ".edn", ".el"
	));

	public static Predicate<String> validLispFiles = string -> {
		for (String fileEnd : validFileEndings) {
			if (string.endsWith(fileEnd)) return true;
		}
		return false;
	};

	public static String headerStart = ";;";

	private static final ScriptEngineFactory FACTORY = new MultiAbclScriptEngineFactory();

	/*public static Interpreter getInterpreter() {
		Interpreter interpreter = Interpreter.createInstance();
		if (interpreter == null) {
			if (Interpreter.getInstance() == null) {
				SeventhSonLogger.fatal("Lisp Interpreter is broken, we're gonna die now...");
			}
			return Interpreter.getInstance();
		} else {
			return interpreter;
		}
	}*/

	public static ScriptEngine getScriptEngine(EntityOrBlockEntity caster, double manaFactor, Object self) {
		if (Lisp.initialized) { //Prevent nasty error messages on first execution.
			clearLisp(); //Make sure variables die between executions. This library is statically implemented, so we have to reset it all the time...
		}
		ScriptEngine engine = FACTORY.getScriptEngine();
		engine.setContext(new PlayerScriptContext(caster, manaFactor, self));
		return engine;
	}

	public static String truncate(String[] strings) {
		StringBuilder builder = new StringBuilder();
		for (String line : strings) {
			builder.append(line).append('\n');
		}
		return builder.toString();
	}
	public static boolean hasValidHeader(String[] text) {
		return text.length > 0 && LispHelper.isValidHeader(text[0]);
	}
	public static boolean isValidHeader(String firstLine) {
		if (firstLine.startsWith(headerStart)) {
			for (String headers : ConfigCommons.getInstance().lispHeaders) {
				if (firstLine.toLowerCase().contains(headers.toLowerCase())) {
					return true;
				}
			}
		}
		return false;
	}

	/*public static boolean parseLisp(PlayerEntity player, ItemStack stack) {
		ItemText text = TextRegistry.getInstance().get(stack).orElse(null);
		if (text != null) {
			if (!player.world.isClient()) {
				try {
					double manaFactor = text.getManaFactor(stack);

					LispHelper.getScriptEngine((ServerPlayerEntity) player,manaFactor).eval(ItemLispCode.codeToRunCommon);
					LispHelper.getScriptEngine((ServerPlayerEntity) player,manaFactor).eval(ItemLispCode.codeToRunItem);

					String lispCode = LispHelper.truncate(text.getText(stack));

					LispHelper.getScriptEngine((ServerPlayerEntity) player, manaFactor).eval(lispCode);
					//System.out.println(LispHelper.getScriptEngine().getContext().getScopes());
				} catch (Throwable e) {
					ExceptionHelper.printExceptionToChat(player, e);
				}
			}
			return true;
		}
		return false;
	}

	public static boolean parseLisp(PlayerEntity player, World world, BlockPos pos) {
		BlockText text = TextRegistry.getInstance().get(world, pos).orElse(null);
		if (text != null) {
			if (!player.world.isClient()) {
				try {
					double manaFactor = text.getManaFactor(world, pos);
					LispHelper.getScriptEngine((ServerPlayerEntity) player,manaFactor).eval(BlockLispCode.codeToRunCommon);
					LispHelper.getScriptEngine((ServerPlayerEntity) player,manaFactor).eval(BlockLispCode.CodeToRunBlock);
					String lispCode = LispHelper.truncate(text.getText(world, pos));
					LispHelper.getScriptEngine((ServerPlayerEntity) player, manaFactor).eval(lispCode);
					//System.out.println(LispHelper.getScriptEngine().getContext().getScopes());
				} catch (Throwable e) {
					ExceptionHelper.printExceptionToChat(player, e);
				}
			}
			return true;
		}
		return false;
	}

	public static boolean parseLisp(PlayerEntity player, Entity entity) {
		EntityText text = TextRegistry.getInstance().get(entity).orElse(null);
		if (text != null) {
			if (!player.world.isClient()) {
				try {
					LispHelper.getScriptEngine((ServerPlayerEntity) player, text.getManaFactor(entity)).eval(LispHelper.truncate(text.getText(entity)));
					//System.out.println(LispHelper.getScriptEngine().getContext().getScopes());
				} catch (Throwable e) {
					ExceptionHelper.printExceptionToChat(player, e);
				}
			}
			return true;
		}
		return false;
	}*/

	public static boolean parseLisp(PlayerEntity player, ItemStack stack) {
		return parseLisp(player, ItemStack.class, stack);
	}

	public static boolean parseLisp(PlayerEntity player, World world, BlockPos pos) {
		return parseLisp(player, new BlockReference(world, pos));
	}

	public static boolean parseLisp(PlayerEntity player, BlockReference reference) {
		return parseLisp(player, BlockReference.class, reference);
	}

	public static boolean parseLisp(PlayerEntity player, Entity entity) {
		return parseLisp(player, Entity.class, entity);
	}

	public static <T> boolean parseLisp(PlayerEntity player, Class<T> objectClass, T object) {
		return parseLisp(new EntityOrBlockEntity(player), objectClass, object);
	}

	public static <T> boolean parseLisp(EntityOrBlockEntity caster, Class<T> objectClass, T object) {
		TextGetter<T> text = TextRegistry.getInstance().get(objectClass, object).orElse(null);
		if (text != null) {
			if (caster.getWorld() != null && !caster.getWorld().isClient()) {
				try {
					String[] strings = new String[] {
							LispHelper.truncate(ExtraLispCodeCallback.getCallback(objectClass).invoker().getAdditionalCode(caster, object)),
							LispHelper.truncate(text.getText(object))
					};
					int max = (int) Math.round(strings[1].split("[(]").length-1 / 2d);
					caster.getPlayer().flatMap(PlayerManaComponent::getComponent).ifPresent(mana -> {
						mana.gainExperience(caster.getRandom().nextInt(Math.max(max, 1)) + 1);
					});
					LispHelper.getScriptEngine(caster, text.getManaFactor(object), object).eval(LispHelper.truncate(strings));
				} catch (Throwable e) {
					ExceptionHelper.printExceptionToChat(caster, e);
				}
			}
			return true;
		}
		return false;
	}

	/*static {
		ENGINE.setBindings(createBindings(), ScriptContext.ENGINE_SCOPE);
		ENGINE.setBindings(createBindings(), ScriptContext.GLOBAL_SCOPE);
	}*/

	public static String[] loadLispFiles(EntityOrBlockEntity caster, String folderName) {
		return loadConfigFiles(((AccessorMinecraftServer)caster.getServer()).getServerResourceManager().getResourceManager(), folderName, validLispFiles, StandardCharsets.UTF_8);
	}


	public static String[] loadConfigFiles(ResourceManager resourceManager, String folderName, Predicate<String> filePredicate, Charset charset) {
		List<String> lines = new ArrayList<>();
		Collection<Identifier> files = resourceManager.findResources(folderName, filePredicate);
		for (Identifier id : files) {
			try {
				Collection<Resource> resources = resourceManager.getAllResources(id);
				for (Resource resource : resources) {
					lines.add(IOUtils.toString(resource.getInputStream(), charset));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File folder = FabricLoader.getInstance().getConfigDir().resolve(folderName).toFile();
		if ((folder.exists() || folder.mkdirs()) && folder.isDirectory()) {
			for (File file : FileHelper.getFilesInFolderAndSubFolders(folder)) {
				if (filePredicate.test(file.getName())) {
					try {
						lines.add(IOUtils.toString(new FileInputStream(file), charset));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return lines.toArray(new String[]{});
	}

	public static LispObject errorNotEntity() {
		return Lisp.error(new ParseError("Caster was not an entity! This spell can only be cast by entities!"));
	}

	public static LispObject errorNotLivingEntity() {
		return Lisp.error(new ParseError("Caster was not a living entity! This spell can only be cast by living entities!"));
	}

	public static LispObject errorNotPlayer() {
		return Lisp.error(new ParseError("Caster was not a player! This spell can only be cast by players!"));
	}

	public static LispObject errorNotBlock() {
		return Lisp.error(new ParseError("Caster was not a block entity! This spell can only be cast by block entities!"));
	}

	public static void clearLisp() {
		for (Package lispPackage : Packages.getAllPackages()) {
			Packages.deletePackage(lispPackage);
		}

		instantiateLisp();
	}

	/*protected static Multimap<Package, Symbol> getDefaultSymbols() {
		Multimap<Package, Symbol> map = HashMultimap.create();
		for (Field field : Symbol.class.getFields()) {
			if (field.getType().equals(Symbol.class)) {
				try {
					Symbol symbol = (Symbol) field.get(null);
					if (symbol.getPackage() instanceof Package) {
						map.put((Package) symbol.getPackage(), symbol);
					}
				} catch (IllegalAccessException | NullPointerException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}*/


	public static List<Symbol> getSymbols(Field[] symbolFields) {
		List<Symbol> list = new ArrayList<>(symbolFields.length);
		for (Field field : symbolFields) {
			field.setAccessible(true);
			if (field.getType().equals(Symbol.class)) {
				try {
					list.add((Symbol) field.get(null));
				} catch (IllegalAccessException | NullPointerException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public static List<Symbol> getDefaultSymbols() {
		return getSymbols(Symbol.class.getFields());
	}

	public static InternalsAndExternals getInternalAndExternalSymbolsSeparately(List<Symbol> all, List<Symbol> internals) {
		List<Symbol> externals = new ArrayList<>(all);
		externals.removeAll(internals);
		return new InternalsAndExternals(internals, externals);
	}

	public static InternalsAndExternals getInternalAndExternalSymbolsSeparatelyByStrings(List<Symbol> all, List<String> internalStrings) {
		return getInternalAndExternalSymbolsSeparately(all, all.stream().filter(symbol -> internalStrings.contains(symbol.getName())).collect(Collectors.toList()));
	}

	public static InternalsAndExternals getDefaultInternalAndExternalSymbolsSeparately() {
		return getInternalAndExternalSymbolsSeparately(getDefaultSymbols(), getDefaultInternalSymbols());
	}

	public static List<Symbol> getDefaultInternalSymbols() {
		List<Symbol> symbols = new ArrayList<>();
		symbols.add(Symbol.CLASS_LAYOUT);
		symbols.add(Symbol.METHOD_COMBINATION_NAME);
		symbols.add(Symbol.METHOD_COMBINATION_DOCUMENTATION);
		symbols.add(Symbol.__SOURCE);
		symbols.add(Symbol.ALLOCATION);
		symbols.add(Symbol.ALLOCATION_CLASS);
		symbols.add(Symbol.ARGUMENT_PRECEDENCE_ORDER);
		symbols.add(Symbol.BACKQUOTE_MACRO);
		symbols.add(Symbol.CASE_FROB_STREAM);
		symbols.add(Symbol.CAUSE);
		symbols.add(Symbol.COMMA_MACRO);
		symbols.add(Symbol.DATUM);
		symbols.add(Symbol.DECLARATIONS);
		symbols.add(Symbol.DEFTYPE_DEFINITION);
		symbols.add(Symbol.EXPECTED_TYPE);
		symbols.add(Symbol.FAST_FUNCTION);
		symbols.add(Symbol.FORMAT_ARGUMENTS);
		symbols.add(Symbol.FORMAT_CONTROL);
		symbols.add(Symbol.FSET);
		symbols.add(Symbol._FUNCTION);
		symbols.add(Symbol.FUNCTION_PRELOAD);
		symbols.add(Symbol._GENERIC_FUNCTION);
		symbols.add(Symbol.INITARGS);
		symbols.add(Symbol.INITFORM);
		symbols.add(Symbol.INITFUNCTION);
		symbols.add(Symbol.INITIAL_METHODS);
		symbols.add(Symbol.INSTANCE);
		symbols.add(Symbol.JAVA_STACK_FRAME);
		symbols.add(Symbol.KEYWORDS);
		symbols.add(Symbol.LAMBDA_LIST);
		symbols.add(Symbol.LISP_STACK_FRAME);
		symbols.add(Symbol.LOCATION);
		symbols.add(Symbol.MACROEXPAND_MACRO);
		symbols.add(Symbol.MAKE_FUNCTION_PRELOADING_CONTEXT);
		symbols.add(Symbol.METHOD_CLASS);
		symbols.add(Symbol._METHOD_COMBINATION);
		symbols.add(Symbol.METHODS);
		symbols.add(Symbol.OBJECT);
		symbols.add(Symbol.OPERANDS);
		symbols.add(Symbol.OPERATION);
		symbols.add(Symbol.OPTIONAL_ARGS);
		symbols.add(Symbol.OTHER_KEYWORDS_P);
		symbols.add(Symbol.PROXY_PRELOADED_FUNCTION);
		symbols.add(Symbol.QUALIFIERS);
		symbols.add(Symbol.READERS);
		symbols.add(Symbol.REQUIRED_ARGS);
		symbols.add(Symbol._SOURCE);
		symbols.add(Symbol.SOCKET_STREAM);
		symbols.add(Symbol.SPECIALIZERS);
		symbols.add(Symbol.STRING_INPUT_STREAM);
		symbols.add(Symbol.STRING_OUTPUT_STREAM);
		symbols.add(Symbol.SYSTEM_STREAM);
		symbols.add(Symbol.STACK_FRAME);
		symbols.add(Symbol._TYPE);
		symbols.add(Symbol.WRITERS);
		return symbols;
	}

	protected static void instantiateLisp() {

		//I think this is why people avoid global variables... I just wanted to reset the script engine...
		AccessorLisp.setPackageCl(Packages.createPackage("COMMON-LISP", 2048));
		AccessorLisp.setPackageClUser(Packages.createPackage("COMMON-LISP-USER", 1024));
		AccessorLisp.setPackageKeyword(Packages.createPackage("KEYWORD", 1024));
		AccessorLisp.setPackageSys(Packages.createPackage("SYSTEM", 2048));
		AccessorLisp.setPackageMop(Packages.createPackage("MOP", 512));
		AccessorLisp.setPackageTpl(Packages.createPackage("TOP-LEVEL", 128));
		AccessorLisp.setPackageExt(Packages.createPackage("EXTENSIONS", 256));
		AccessorLisp.setPackageJvm(Packages.createPackage("JVM", 2048));
		AccessorLisp.setPackageLoop(Packages.createPackage("LOOP", 512));
		AccessorLisp.setPackageProf(Packages.createPackage("PROFILER"));
		AccessorLisp.setPackageJava(Packages.createPackage("JAVA"));
		AccessorLisp.setPackageLisp(Packages.createPackage("LISP"));
		AccessorLisp.setPackageThreads(Packages.createPackage("THREADS"));
		AccessorLisp.setPackageFormat(Packages.createPackage("FORMAT"));
		AccessorLisp.setPackageXp(Packages.createPackage("XP"));
		AccessorLisp.setPackagePrecompiler(Packages.createPackage("PRECOMPILER"));
		AccessorLisp.setPackageSequence(Packages.createPackage("SEQUENCE", 128));
		

		//Run static initialization code.
		Lisp.PACKAGE_CL.addNickname("CL");
		Lisp.PACKAGE_CL_USER.addNickname("CL-USER");
		Lisp.PACKAGE_CL_USER.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_CL_USER.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_CL_USER.usePackage(Lisp.PACKAGE_JAVA);
		Lisp.PACKAGE_SYS.addNickname("SYS");
		Lisp.PACKAGE_SYS.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_SYS.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_MOP.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_MOP.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_MOP.usePackage(Lisp.PACKAGE_SYS);
		Lisp.PACKAGE_TPL.addNickname("TPL");
		Lisp.PACKAGE_TPL.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_TPL.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_EXT.addNickname("EXT");
		Lisp.PACKAGE_EXT.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_EXT.usePackage(Lisp.PACKAGE_THREADS);
		Lisp.PACKAGE_JVM.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_JVM.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_JVM.usePackage(Lisp.PACKAGE_SYS);
		Lisp.PACKAGE_LOOP.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_PROF.addNickname("PROF");
		Lisp.PACKAGE_PROF.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_PROF.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_JAVA.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_JAVA.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_LISP.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_LISP.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_LISP.usePackage(Lisp.PACKAGE_SYS);
		Lisp.PACKAGE_THREADS.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_THREADS.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_THREADS.usePackage(Lisp.PACKAGE_SYS);
		Lisp.PACKAGE_FORMAT.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_FORMAT.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_XP.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_PRECOMPILER.addNickname("PRE");
		Lisp.PACKAGE_PRECOMPILER.usePackage(Lisp.PACKAGE_CL);
		Lisp.PACKAGE_PRECOMPILER.usePackage(Lisp.PACKAGE_EXT);
		Lisp.PACKAGE_PRECOMPILER.usePackage(Lisp.PACKAGE_SYS);
		Lisp.PACKAGE_SEQUENCE.usePackage(Lisp.PACKAGE_CL);

		InternalsAndExternals symbols = getDefaultInternalAndExternalSymbolsSeparately();
		InternalsAndExternals lispSymbols = getInternalAndExternalSymbolsSeparately(getSymbols(Lisp.class.getDeclaredFields()), ImmutableList.of(
				Lisp.STANDARD_READTABLE,
				Lisp.DOUBLE_COLON_PACKAGE_SEPARATORS,
				Lisp._LOAD_DEPTH_,
				Lisp._LOAD_STREAM_,
				Lisp.AUTOLOADING_CACHE,
				Lisp._PRINT_FASL_,
				Lisp._BACKQUOTE_COUNT_,
				Lisp._BQ_VECTOR_FLAG_
		));
		InternalsAndExternals loadSymbols = getInternalAndExternalSymbolsSeparatelyByStrings(getSymbols(Load.class.getDeclaredFields()), ImmutableList.of(
				"*FASL-LOADER*",
				"*FASL-VERSION*",
				"*FASL-EXTERNAL-FORMAT*",
				"*FASL-UNINTERNED-SYMBOLS*"
		));

		for (Field packageField : Lisp.class.getFields()) {
			if (packageField.getType().equals(Package.class)) {
				try {
					Package lispPackage = (Package) packageField.get(null);
					ConcurrentHashMap<String, Symbol> externalMap = ((AccessorPackage) (Object) lispPackage).getExternalSymbols();
					ConcurrentHashMap<String, Symbol> internalMap = ((AccessorPackage) (Object) lispPackage).getInternalSymbols();

					//Deal with normal symbols.
					addSymbols(lispPackage, symbols.externals, externalMap);
					addSymbols(lispPackage, symbols.internals, internalMap);
					addSymbols(lispPackage, getSymbols(JavaStackFrame.class.getDeclaredFields()), internalMap);

					addSymbols(lispPackage, lispSymbols.externals, externalMap);
					addSymbols(lispPackage, lispSymbols.internals, internalMap);
					addSymbols(lispPackage, getSymbols(Extensions.class.getFields()), externalMap);
					addSymbols(lispPackage, getSymbols(Stream.class.getDeclaredFields()), internalMap);

					addSymbols(lispPackage, loadSymbols.externals, externalMap);
					addSymbols(lispPackage, loadSymbols.internals, internalMap);
					addSymbols(lispPackage, getSymbols(Keyword.class.getDeclaredFields()), internalMap);
					addSymbols(lispPackage, getSymbols(Site.class.getDeclaredFields()), externalMap);
					addSymbols(lispPackage, getSymbols(Debug.class.getDeclaredFields()), externalMap);

					addSymbols(lispPackage, ImmutableList.of(JavaObject.JAVA_CLASS_JCLASS, JavaObject.JAVA_CLASS, JavaObject.ENSURE_JAVA_CLASS), internalMap);
					addSymbols(lispPackage, ImmutableList.of(JavaObject._JAVA_OBJECT_TO_STRING_LENGTH), externalMap);

					addSymbols(lispPackage, getSymbols(LogicalPathname.class.getDeclaredFields()), externalMap);

					addSymbols(lispPackage, getSymbols(AutoloadGeneralizedReference.class.getDeclaredFields()), internalMap);
					addSymbols(lispPackage, ImmutableList.of(JavaClassLoader.CLASSLOADER), internalMap);

					//Deal with primitives.
					fixFunctions(Primitives.class.getDeclaredFields(), lispPackage, Primitive.class, SpecialOperator.class);
					fixFunctions(ArithmeticError.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(Function.class.getFields(), lispPackage, Primitive.class);
					fixFunctions(HashTableFunctions.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(Lisp.class.getDeclaredFields(), lispPackage, Primitive.class, SpecialOperator.class);
					fixFunctions(PrintNotReadable.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(TypeError.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(arglist.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(make_condition.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(Extensions.class.getDeclaredFields(), lispPackage, Primitive.class, SpecialOperator.class);
					fixFunctions(Do.class.getDeclaredFields(), lispPackage, SpecialOperator.class);
					fixFunctions(SpecialOperators.class.getDeclaredFields(), lispPackage, SpecialOperator.class);
					fixFunctions(LispThread.class.getDeclaredFields(), lispPackage, Primitive.class, SpecialOperator.class);
					fixFunctions(ByteArrayInputStream.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(Environment.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(AutoloadGeneralizedReference.class.getDeclaredFields(), lispPackage, Primitive.class);
					fixFunctions(JavaClassLoader.class.getDeclaredFields(), lispPackage, Primitive.class);

				} catch (IllegalAccessException | NullPointerException e) {
					e.printStackTrace();
				}
			}
		}

		Interpreter.interpreter = null;
		Lisp.initialized = false;
	}

	protected static void fixFunctions(Field[] fields, Package lispPackage, Class<?>... types) {
		for (Field operatorField : fields) {
			boolean success = false;
			for (Class<?> type : types) {
				if (operatorField.getType().equals(type)) {
					success = true;
					break;
				}
			}
			if (success) {
				try {
					operatorField.setAccessible(true);
					Operator primitive = (Operator) operatorField.get(null);
					LispFunctionCapability cap = (LispFunctionCapability) primitive;
					if (primitive.getLambdaName() instanceof Symbol) {
						Symbol symbol = (Symbol) primitive.getLambdaName();
						if (symbol.getPackage() instanceof Package && ((Package) symbol.getPackage()).getName().equals(lispPackage.getName())) {
							SeventhSonLogger.info("Package: " + lispPackage.getName() + "  Symbol: " + symbol.getName() + "  State: " + cap.getSymbolState());
							if (cap.getSymbolState().shouldAddSymbol()) {
								AccessorPackage lispPackageAccessor = (AccessorPackage) (Object) lispPackage;
								boolean exported;
								if (lispPackage == PACKAGE_KEYWORD) {
									symbol.initializeConstant(symbol);
									exported = true;
								} else {
									exported = cap.getSymbolState().isExported();
								}
								if (exported) {
									lispPackageAccessor.getExternalSymbols().put(symbol.getName(), symbol);
								} else {
									lispPackageAccessor.getInternalSymbols().put(symbol.getName(), symbol);
								}
							}
						}
					}
				} catch (IllegalAccessException | NullPointerException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected static void addSymbols(Package lispPackage, List<Symbol> symbols, ConcurrentHashMap<String, Symbol> symbolMap) {
		for (Symbol symbol : symbols) {
			if (symbol.getPackage() instanceof Package) {
				Package symbolPackage = (Package) symbol.getPackage();
				if (symbolPackage.getName().equals(lispPackage.getName())) {
					symbolMap.put(symbol.getName(), symbol);
					((AccessorSymbol) symbol).setPkg(lispPackage);
				}
			}
		}
	}
	/*public static Package clonePackage(Package lispPackage) {
		Package newPackage = new Package(lispPackage.getName());
		newPackage.setPropertyList(lispPackage.getPropertyList().clone);
		return lispPackage;
	}

	public static void cacheLispData() {
		for (Field field : Lisp.class.getFields()) {
			if (field.getType().isInstance(Package.class)) {
				try {
					cachedLispPackages.put(field, (Package) field.get(null));
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void resetLisp() {

	}*/

	public static class MultiAbclScriptEngineFactory extends AbclScriptEngineFactory {

		@Override
		public synchronized ScriptEngine getScriptEngine() {
			return new AbclScriptEngine() {};
		}
	}

	public static class InternalsAndExternals {
		public final List<Symbol> internals;
		public final List<Symbol> externals;

		public InternalsAndExternals(List<Symbol> internals, List<Symbol> externals) {
			this.internals = internals;
			this.externals = externals;
		}
	}

}
