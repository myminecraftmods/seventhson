package it.lundstedt.erik.seventhSon.util.helper;

import it.lundstedt.erik.seventhSon.common.registries.TypeRegistry;
import net.minecraft.network.packet.s2c.play.EntityAttributesS2CPacket;
import net.minecraft.util.registry.Registry;

import java.util.Map;
import java.util.stream.Collectors;

public class RegistryHelper {

	public static <T> Iterable<T> getValues(Registry<T> registry) {
		return registry.getEntries().stream().map(Map.Entry::getValue).collect(Collectors.toSet());
	}

}
