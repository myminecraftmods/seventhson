package it.lundstedt.erik.seventhSon.util.helper;

import net.fabricmc.fabric.api.dimension.v1.FabricDimensions;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.TeleportTarget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class TeleportHelper {

	//TODO Requiem compat
	public static Entity teleport(Entity entity, ServerWorld destWorld, TeleportTarget destPos) {
		if (entity.hasVehicle()) {
			teleport(entity.getVehicle(), destWorld, destPos);
			return entity;
		}
		Collection<Entity> passengers = new HashSet<>();
		if (entity.hasPassengers()) {
			List<Entity> prevPassengers = new ArrayList<>(entity.getPassengerList());
			for (Entity passenger : prevPassengers) {
				passenger.stopRiding();
				passengers.add(teleport(passenger, destWorld, destPos));
			}
		}
		entity = teleportEntity(entity, destWorld, destPos);
		for (Entity passenger : passengers) {
			passenger.startRiding(entity);
		}
		return entity;
	}

	private static Entity teleportEntity(Entity entity, ServerWorld destWorld, TeleportTarget destPos) {
		if (entity instanceof ServerPlayerEntity) {
			((ServerPlayerEntity)entity).teleport(destWorld, destPos.position.getX(), destPos.position.getY(), destPos.position.getZ(), destPos.yaw, destPos.pitch);
			entity.setVelocity(destPos.velocity);
			entity.velocityDirty = true;
			entity.velocityModified = true;
			return destWorld.getPlayerByUuid(entity.getUuid());
		} else {
			if (!entity.world.getRegistryKey().equals(destWorld.getRegistryKey())) {
				return FabricDimensions.teleport(entity, destWorld, destPos);
			} else {
				entity.requestTeleport(destPos.position.getX(), destPos.position.getY(), destPos.position.getZ());
				entity.setVelocity(destPos.velocity);
				entity.velocityDirty = true;
				entity.velocityModified = true;
				entity.yaw = destPos.yaw;
				entity.pitch = destPos.pitch;
				return entity;
			}
		}
	}

}
